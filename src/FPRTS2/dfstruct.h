#pragma once

#include "datafragment.h"

namespace fprts {

class DFStructArray1D: public DFStruct  // 1d array
{
public:
	map<unsigned int, DataFragment*> elems; // map of elems
	unsigned int size; // size of array

	//map<Range, NodeId> distr; // preliminary block distribution
	map<unsigned int, NodeId> mig_distr; // new distr of elems that migrated

public:		
	DFStructArray1D(FragmentId i) : DFStruct(i), size(0) {}
	DFStructArray1D(FragmentId i, int sz) : DFStruct(i), size(sz) {}
	virtual ~DFStructArray1D() {}

	DataFragment::Type GetType() const {return DataFragment::STRUCT_ARRAY_1D;}

	unsigned int Dims() const {return 1;}
	unsigned int GetSize() const {return size;}

	DataFragment* GetElem(unsigned int num); // get children
	void AddElem(unsigned int num, DataFragment *df); // add children

	unsigned int Weight() {return size;}

	//void Pack(ReadWriteBuffer &buf); // pack itself to buffer
	//void Unpack(ReadWriteBuffer &buf); // unpack itself from buffer
};

class DFStructArray2D: public DFStruct  // 2d array
{
};

class DFStructRecord : public DFStruct // record (struct)
{
public:		
	DataFragment::Type GetType() const {return DataFragment::RECORD;}
};

/*
///////////////////////// Structured DF ////////////////////////
class StructuredDataFragment : public DataFragment // structired DF
{
public:
	StructuredDataFragment() {}
	~StructuredDataFragment() {}
	
	virtual Fragment* GetChild(const Param& tag) = 0; // get child

	virtual bool IsStructured() {return true;} // check if it's structured fragment
};

///////////////////////// Structured DF Struct ////////////////////////
class StructuredDFStruct : public StructuredDataFragment // structired struct
{
private:
	Map<string, Fragment*> fields; // fields of structure

protected:
	void RegisterField(const string& name, Fragment* f); // register field location 

public:
	StructuredDFStruct() {}
	~StructuredDFStruct() {}
	
	virtual Fragment* GetChild(const Param& tag); // get child
};

//////////////////////// Structured Array //////////////////////
/*template<class T>
class StructuredDFVector : public StructuredDataFragment // vector of fragments
{
protected:	
	typename vector<T> elems;

public:
	StructuredDFVector() {}
	StructuredDFVector(const UInt& sz)
	{
		elems.reserve(sz);		
		for(UInt i = 0;i < sz;i++) elems.insert(elems.begin(), sz, T());
	}
	~StructuredDFVector() {}

	//T* Data() {return (T*)GetBuf();}
	UInt GetSize() const {return elems.size();}

	virtual void Pack(ReadWriteBuffer& buf) 
	{		
		buf << elems.size();
		for(typename vector<T>::iterator it = elems.begin();it != elems.end();it++) // delete elems of vector
		{
			(*it).Pack(buf);
		}
	}
	virtual void Unpack(ReadWriteBuffer& buf) 
	{
		UInt sz;
		buf >> sz;
		for(UInt i = 0;i < sz;i++) // delete elems of vector
		{			
			elems.push_back(T());
			elems[elems.size() - 1].Unpack(buf);
		}
	}

	virtual Fragment* Spawn(ParamArray& arg) {return new DFStructuredVector<T>((*params)[0]);}
	virtual void Copy(DataFragment* df)
	{	
		DFStructuredVector<T>* src = (DFStructuredVector<T>*)df;
		for(UInt i = 0;i < elems.GetSize();i++) 
		{
			elems[i]->Copy(src[i]);
		}		
	}

	T& operator[](const UInt& ind) {return *(elems[ind]);}

	virtual bool IsStructured() {return true;} // check if it's structured fragment	

	virtual void Expand(FragmentScope* scope, vector<int>* params = 0) // for structured fragments
	{
		for(UInt i = 0;i < elems.size();i++)
		{
			scope->AddDF<T>();
		}
	}	
};*/

};