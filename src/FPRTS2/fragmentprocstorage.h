#pragma once

#include "fragmentprocedure.h"

namespace fprts {

class FragmentProcStorage // storage of fragment procs on node
{
protected:
	map<ProcedureId, boost::shared_ptr<FragmentProcedure> > procs;	
protected:
	FragmentId proc_cnt;
	NodeId rank;
public:
	FragmentProcStorage() : proc_cnt(0), rank(0) {}
	FragmentProcStorage(NodeId rnk) : proc_cnt(0), rank(rnk) {}
	~FragmentProcStorage() {}

	void Init(NodeId rnk) {rank = rnk;}

	FragmentProcedure* NewProc();
	void AddProc(FragmentProcedure *proc);
	FragmentProcedure* GetProc(ProcedureId id);
	void RemoveProc(ProcedureId id);

	bool ProcExists(ProcedureId id) const;
	bool IsEmpty() const {return procs.empty();}
};

}
