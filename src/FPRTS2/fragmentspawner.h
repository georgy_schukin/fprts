#include "rtstypes.h"
#include "codevertex.h"
#include "context.h"

namespace fprts {

class CodeGraph;
class FragmentProcedure;

class FragmentSpawner // create fragments for procedure
{
public:
	enum Type
	{
		BASIC = 0
	};
protected:
	CodeGraph *graph;
	FragmentProcedure *proc;
public:
	FragmentSpawner(CodeGraph *g, FragmentProcedure *p) : graph(g), proc(p) {}
	~FragmentSpawner() {}

	virtual bool Spawn() = 0;
};

class BasicFragmentSpawner : public FragmentSpawner
{
protected:
	map<VertexId, FragmentId> v_to_f;
protected:
	FragmentId ProcessData(const VertexData &v);
	FragmentId ProcessCall(const VertexCall &v);
	InputArgContext::Arg ResolveArg(const VertexCall::Arg &arg);
public:
	BasicFragmentSpawner(CodeGraph *g, FragmentProcedure *p) : FragmentSpawner(g, p) {}
	~BasicFragmentSpawner() {}

	virtual bool Spawn();
};

class TreeFragmentSpawner : public BasicFragmentSpawner
{
protected:
	int max_depth; // max depth of trees for spawn
	int max_cnt; // max num of fragments for one spawn
public:
	TreeFragmentSpawner(CodeGraph *g, FragmentProcedure *p, int md = 0, int mc = 0) : BasicFragmentSpawner(g, p), max_depth(md), max_cnt(mc) {}
	~TreeFragmentSpawner() {}

	virtual bool Spawn();
};

}
