#pragma once

#include "fragment.h"

#include <map>
#include <vector>
#include <boost/shared_array.hpp>

using namespace std;

namespace fprts {

class DataFragment: public Fragment // abstract fragment of data
{
public:
	enum Type // possible df types
	{
		BLOCK = 0,
		STRUCT_ARRAY_1D,
		RECORD
	};
public:
	DataFragment(FragmentId i) : Fragment(i) {}
	virtual ~DataFragment() {}

	virtual bool IsAtom() const = 0;
	virtual DataFragment::Type GetType() const = 0;

	static DataFragment* New(DataFragment::Type tp, FragmentId i, const vector<int> &params); // spawn new fragment of this type
};

class DFAtom : public DataFragment // atom data fragment
{
public:
	DFAtom(FragmentId i) : DataFragment(i) {}
	virtual ~DFAtom() {}

	bool IsAtom() const {return true;}

	//virtual int Create() = 0; // create = get mem for data
	//virtual bool IsCreated() const = 0; // if fragment is created

	virtual void* Data() const = 0; // data of fragment
	virtual unsigned int GetSize() const = 0; // size of fragment
};

class DFStruct : public DataFragment // complex distributed df (array/struct)
{
public:
	//set<DataFragment*> elems; // current children dfs on the node	
	//vector<DFType> child_types;
	//DFType child_type;
	vector<int> child_params;

public:	
	DFStruct(FragmentId i) : DataFragment(i) {}
	virtual ~DFStruct() {}

	virtual bool IsAtom() const {return false;}
	
	virtual DataFragment* GetElem(unsigned int num) = 0; // get children
	virtual void AddElem(unsigned int num, DataFragment *df) = 0; // add children
};

}
