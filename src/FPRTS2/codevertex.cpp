#include "codevertex.h"

namespace fprts{

void VertexData::SetData(DataFragment::Type type, const vector<int> &params) { // add data decl	
	data_type = type;
	data_params = params;	
}

void VertexCall::SetArgs(const vector<VertexCall::Arg> &a, unsigned int i_sz, unsigned int o_sz) {
	args = a;
	input_sz = i_sz;
	output_sz = o_sz;
}

}