#pragma once

#include "fragmentscope.h"   

namespace rts
{

////////////////////////// FP Graph ////////////////////////////
class FPGraph
{
public:
	/*struct Context
	{
		Map<SystemId, SystemId> df_owners;
		IdSet available_df;
	};*/

public:
	/*class ArcInfo // info about arc
	{
		IdSet dfs; // which dfs to transit
		UInt weight; // total weight of arc
	};*/

	class VertexInfo
	{
		//Map<SystemId, ArcInfo> arcs; // incoming arcs
		//IdSet in_vertexes;
		//IdSet out_vertexes;
		Map<SystemId, IdSet> input_connection;
		Map<SystemId, IdSet> output_connection;
		IdSet input_signals;
		IdSet output_signals;
		//IdSet connected_cfs; // 
		//IdSet edges;
		
		UInt weight;
		UInt priority;
		bool is_scalar;
		//UInt order_weight;

		VertexInfo() : is_scalar(true) {}
	};

public:
	//FragmentScope* scope;
	//FragmentDescr* root;

	Map<SystemId, VertexInfo> cf_vertexes;	
	//Map<SystemId, Map<SystemId, ArcInfo> > arcs;

	Map<SystemId, IdSet> df_init; // first usage of dfs

	Map<SystemId, SystemId> df_owners;
	IdSet available_df;

protected:
	//void ProcessCF(CompFragmentDescr* root, Map<SystemId, SystemId> df_owner, OrderModifier mod);
	//void OpenVertex(CompFragmentDescr* root);

	//void ProcessVertex(CompFragmentDescr* root, Map<SystemId, VertexInfo>& res, Map<SystemId, IdSet>& df_res);

	void ExpandVertex(SystemId vertex_id, CompFragmentDescr* root, bool rec = false);
	void ExpandVertex(CompFragmentDescr* root, bool rec = false);

	void BuildConnections(SystemId vertex_id, CompFragmentDescr* root);
	void BuildDF(SystemId vertex_id, CompFragmentDescr* root);

	void GetUsedDF(CompFragmentDescr* root, Map<SystemId, IdSet>& res);
	void GetUsedDF(CompFragmentDescr* root, IdSet& res);

	void BuildConnections(FragmentDescr* root, const IdSet& start);

	void BuildGraph(FragmentDescr* root, Map<SystemId, SystemId>& df_last_owner);

	void GetFreeCF(FragmentDescr* root, IdSet& cfs);

	//void InitVertexes(FragmentDescr* begin);

public:
	void BuildGraph(FragmentDescr* root); // build graph of a program
};

}