#include "datafragment.h"
#include "dfatom.h"
#include "dfstruct.h"

namespace fprts {

DataFragment* DataFragment::New(DataFragment::Type tp, FragmentId i, const vector<int> &params) {
	switch(tp) {
		case DataFragment::BLOCK: {
			unsigned int sz = (!params.empty()) ? params[0] : 0;
			return new DFAtomBlock(i, sz);
		}
		//case DFT_ARRAY_1D: return new DFStructArray1D();
	}
return 0;
}

}
