#pragma once

#include "topology.h"
#include <mpi.h>
#include <map>
#include <set>
#include <boost/thread/mutex.hpp>

#define MICROMPI

#ifdef MICROMPI
#define MSL boost::unique_lock<boost::mutex> lock(ms_mutex);
//#define MSU ms_mutex.unlock();
#else
#define MSL ;
//#define MSU ;
#endif

namespace fprts {

class MPICommunicator // mpi comm
{
public:
    struct Status
	{
		int rank;
		int size;
		int tag;
	};
    
protected:
	volatile int rank;
	volatile int size;

	std::map<int, MPI_Request> requests;
	int req_id;

	std::set<int> neigh_nodes;

	boost::mutex ms_mutex;
	
public:	
	MPI_Comm mpi_comm; // comm for MPI

protected:
	static void InitMPIComm(Topology::Type tp, int rank, int size, MPI_Comm *comm);
	static void InitNeighbours(Topology::Type tp, int rank, int size, MPI_Comm comm, set<int> &neigh);
	
public:
	MPICommunicator() : rank(0), size(0), req_id(0) {}
	~MPICommunicator() {}

	int Init(Topology::Type tp = Topology::ALL_TO_ALL);
	int Finalize(); 

	int Send(void *data, int data_sz, int data_tag, int dest_node);
	int Recv(void *data, int data_sz, int data_tag, int src_node);

	int Isend(void *data, int data_sz, int data_tag, int dest_node, int &id);

	bool Check(int node, int data_tag, MPICommunicator::Status& stat); 
	bool CheckAny(MPICommunicator::Status& stat);
	bool CheckCompletion(int id);

	void Barrier();
	
	void Reduce(void *send, void *recv, int data_sz, MPI_Op op, int node);
	void Allreduce(void *send, void *recv, int data_sz, MPI_Op op);

	int GetRank() const {return rank;}
	int GetSize() const {return size;}
	const set<int>& GetNeighbours() const {return neigh_nodes;}
};

//////////////////////////// MPI Async Communicator /////////////////////////////////////////
/*class MPIAsyncCommunicator : public MPICommunicator // MPI with async send/recv
{
protected:
	class AsyncData
	{
		public:
			Buffer buf;
			MPI_Request req;

		public:
			AsyncData() {}
			AsyncData(void* b, UInt& sz) : buf(b, sz) {}
			~AsyncData() {}
	};

protected:
	list<AsyncData*> curr_comm;

protected:
	void CheckCurr();

public:
	MPIAsyncCommunicator();
	virtual ~MPIAsyncCommunicator();

	virtual int Send(void* data, UInt data_sz, Int data_tag, NodeId dest_node);	
};*/
}
