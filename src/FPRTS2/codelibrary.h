#pragma once

#include "codegraph.h"
#include "idgen.h"

#include <boost/shared_ptr.hpp>
#include <map>

namespace fprts {

class CodeLibrary // storage of code fragments
{
protected:
	std::map<FragmentId, boost::shared_ptr<CodeFragment> > cdfs;
	
public:
	CodeLibrary() {}
	~CodeLibrary() {}

	template<class T>
	FragmentId Register(const string &name); // reg cdf in library

	FragmentId GetId(const string& name) const; // get id by name

	CodeFragment* GetCode(FragmentId id); // get code by id
	bool Exists(FragmentId id) const; // check if code exists
};

template<class T>
FragmentId CodeLibrary::Register(const string &name) {
	FragmentId id = GetId(name); // get id	
	CodeFragment *code = new T();
	code->Init(*this);
	if(!code->IsAtom()) {
		CodeGraph *graph = (CodeGraph*)code;
		graph->Build(*this); // build graph for struct code
	}
	cdfs[id] = boost::shared_ptr<CodeFragment>(code); // add	
return id;
}

}
