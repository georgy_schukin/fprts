#pragma once

#include "mpicomm.h"
#include "rtstypes.h"
#include "messagesystem.h"
#include "topology.h"
#include "distributor.h"
#include "fragmentgraph.h"
#include "fragmentprocedure.h"
#include "fragmentlocationstorage.h"

#include <vector>
#include <queue>
#include <map>
#include <set>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

namespace fprts {

class DataFragment;
class CompFragment;
class ExecSystem;
//class FragmentGraph;

class CommSystem // high-level system for send/recv fragments and other info between nodes
{
protected:
	ExecSystem *exec_system;
	MessageSystem ms;
	boost::thread s_thread, r_thread;

	boost::mutex ms_mutex;

	volatile bool is_working;

	Topology topology;

protected:
	void SendFunc();
	void RecvFunc();

	void OnMessage(Message *msg);
	void RecvDFs(Message *msg);	
	void RecvCFs(Message *msg);
	void RecvGraph(Message *msg);	
	void RecvDFRequest(Message *msg);
	void RecvDepend(Message *msg);
	void RecvDone(Message *msg);
	void RecvLocation(Message *msg, bool df);

public:
	CommSystem() : is_working(false) {}
	~CommSystem() {}

	MessageSystem& GetMessageSystem() {return ms;}
	Topology& GetTopology() {return topology;}
	
	void SetExecSystem(ExecSystem* ex) {exec_system = ex;}
	
	void SendDFs(ProcedureId proc_id, const FragmentProcedure::DFList &dfs, NodeId node);	
	void SendCFs(ProcedureId proc_id, const FragmentProcedure::CFList &cfs, NodeId node);
	void SendGraph(ProcedureId proc_id, const FragmentGraph &graph, Distributor::DistributionPlan &plan, NodeId node); // send part of graph to node (by distr plan)
	void SendDFRequest(ProcedureId proc_id, const FragmentIdSet &df_ids, NodeId src, NodeId node);	
	void SendDepend(ProcedureId proc_id, const FragmentProcedure::CFDepMap &cf_deps, NodeId node);	
	void SendDone(ProcedureId proc_id, NodeId src, NodeId node);
	void SendLocation(ProcedureId proc_id, const FragmentLocationStorage::LocationNodeMap &loc, bool df, NodeId node);

	void Init(Topology::Type tp); // init comm, etc
	void Finalize(); // finalize comm, etc

	int Start();
	int Stop();
};

}
