#include "fragmentdescr.h"
#include "fragmentscope.h"

namespace rts
{

/////////////////////////// Fragment Descr /////////////////////////
FragmentDescr::FragmentDescr(FragmentScope* s) : 
	fragment_id(0), fragment_type_id(0), fragment_name(""), parent(0), scope(s) 
{
	if(scope) scope->LinkDescr(this); // link to scope
}

FragmentDescr::FragmentDescr(const string& name, SystemId type_id, FragmentDescr* p, FragmentScope* s) :
	fragment_name(name), fragment_type_id(type_id), parent(p), scope(s)
{	
	if(scope) scope->LinkDescr(this); // link to scope
}

/*FragmentDescr::FragmentDescr(const FragmentDescr& descr) : 
	fragment_id(descr.Id()), fragment_type_id(descr.TypeId()), 
	fragment_name(descr.Name()), params(descr.Params()), parent(0) {}*/

FragmentDescr::~FragmentDescr()
{
	if(scope) scope->UnlinkDescr(this); // unlink from scope
	//cout << "Delete " << Name() << endl;
	//if(!parent) // don't have parent - delete children yourself
	/*{
		for(DescrPtrArray::iterator it = dfs.begin();it != dfs.end();it++) 
		{
			if(*it) delete (*it); // delete children
		}
		for(DescrPtrArray::iterator it = cfs.begin();it != cfs.end();it++) 
		{
			if(*it) delete (*it);
		}
	}*/
}

void FragmentDescr::Clear() // unlink children
{
	dfs.clear();
	cfs.clear();
	SetParent(0);
}

FragmentDescr* FragmentDescr::FindDF(const string& name) const
{
	for(DescrPtrArray::const_iterator it = dfs.begin();it != dfs.end();it++)
	{
		if((*it)->Name() == name) return (*it);
	}
return 0;
}

FragmentDescr* FragmentDescr::FindCF(const string& name) const
{
	for(DescrPtrArray::const_iterator it = cfs.begin();it != cfs.end();it++)
	{
		if((*it)->Name() == name) return (*it);
	}
return 0;
}

bool FragmentDescr::AddDF(FragmentDescr *df)
{
	//if(ExistsDF(df->Name())) return false;
	df->SetParent(this);
	dfs.push_back(df);
return true;
}

bool FragmentDescr::AddCF(FragmentDescr *cf)
{	
	//if(ExistsCF(cf->Name())) return false;
	cf->SetParent(this);	
	cfs.push_back(cf);
return true;
}

string FragmentDescr::GetFullName() const 
{
	string res = Name();
	FragmentDescr* p = Parent();
	while(p)
	{
		res = p->Name() + "::" + res;
		p = p->Parent();
	}
return res;
}


////////////////////// Func CF //////////////////////////////////
/*FuncCFDescr::FuncCFDescr(const FuncCFDescr& descr) : ScalarCFDescr(descr) 
{
	for(UInt i = 0;i < descr.args.size();i++)
	{
		args.push_back((DataFragmentDescr*)FragmentScope::CopyDescrTree(descr.args[i]));
		access.push_back(descr.access[i]);
	}
}*/

void FuncCFDescr::AddArg(const string& name, SystemId type_id, AccessType ac)
{	
	args.push_back(new DataFragmentDescr(name, type_id, 0));
	access.push_back(ac);
}

DataFragmentDescr* FuncCFDescr::Arg(const string& name) const
{
	for(vector<DataFragmentDescr*>::const_iterator it = args.begin();it != args.end();it++)
	{
		if((*it)->Name() == name) return *it;
	}
return 0;
}


/*void AddArgument(const string& name, SystemId type, AccessType ac)
{
	args.push_back(ArgInfo());
	ArgInfo& arg = args[args.size() - 1];

	arg.name = name;
	arg.type_id = type_id;
	arg.ac_type = ac;
}*/

}