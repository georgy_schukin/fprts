#pragma once

#include "fragment.h"

namespace rts
{

typedef std::vector<DataFragment*> FuncArgs;
typedef int (*FUNC)(FuncArgs);

/*class BaseFragmentProgram // base for fragment program
{
public:
	DFCollection dfs; // data fragments
	CFCollection cfs; // comp fragments

	Collection<FUNC> code_frags;

	DFCollection df_spawners; // for df creation

	Map<SystemId, IdSet> init_send; // df -> cfs

public:
	BaseFragmentProgram() {}
	~BaseFragmentProgram() {}
};*/

class FragmentProgram : public BaseFragmentProgram
{
public:
	Map<SystemId, IdSet> df_neighbours; // neighbour dfs

	Map<SystemId, IdSet> cf_order; // order on cfs
	Map<SystemId, IdSet> cf_order_reverse; // reverse order on cfs

	Map<SystemId, Int> df_priorities; // priorities of df
	Map<SystemId, Int> cf_priorities; // priorities of cf

	Map<SystemId, IdSet> groups; // groups of fragments

protected:
	void OptimizeDF(DataFragment* df);

	/*void Intersection(const set<SystemId>& s1, const set<SystemId>& s2, set<SystemId>& res);
	void Difference(const set<SystemId>& s1, const set<SystemId>& s2, set<SystemId>& res);

	void BuildPath(const SystemId& cf_id, Map<SystemId, SystemId>& last_df_owner);*/
	void BuildConnections(const IdSet& start);
	//void Remove(const SystemId& cf_id, set<SystemId>& sended);

public:
	void GetFreeCF(IdSet& free_cf);
	void GetNextCF(const IdSet& cf_layer, IdSet& cf_next_layer);

	void BuildGraph(); // build graph of a program
	void Optimize(); // do optimizations with program

	int AddDF(const SystemId& df_id, DataFragment* df, const SystemId& type_id);
	int AddCF(const SystemId& cf_id, const SystemId& code_id, const UInt& p_num, Param* params, const UInt& weight = 1, const UInt& prior = 1);

	void SetOrder(const SystemId& cf_id_first, const SystemId& cf_id_second);	

public:
	FragmentProgram() {}
	~FragmentProgram()
	{
		for(DFCollection::iterator it = dfs.begin();it != dfs.end();it++) delete it->second;
		for(DFCollection::iterator it = df_spawners.begin();it != df_spawners.end();it++) delete it->second;
		for(CFCollection::iterator it = cfs.begin();it != cfs.end();it++) delete it->second;
	}
};

void SortByAccess(BaseFragmentProgram* fp, const IdSet& cfs, const SystemId& df_id, list<SystemId>& res);

}