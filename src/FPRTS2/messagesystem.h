#pragma once

#include "messagestorage.h"

#include <boost/thread/mutex.hpp>

using namespace std;

namespace fprts {

class MessageSystem // system to communicate among nodes with messages
{
protected:
	MessageStorage storage;

	queue<pair<unsigned int, NodeId> > tasks; // send tasks
	map<unsigned int, int> access; // num of accesses to message	
	set<unsigned int> free_flg; // ids of messages that were freed
	map<int, unsigned int> isends; // request -> msg id : ongoing isends

	boost::mutex ms_mutex;

public:
	MessageSystem() {}
	~MessageSystem() {}

	void Init(unsigned int sz, unsigned int msg_sz);

	Message* Get(Message::Tag msg_tag);
	void Free(Message *msg);

	int Send(Message *msg, NodeId node);
	int SendAndFree(Message *msg, NodeId node);

	bool DoSend();
	void CheckAll();
};

}