#pragma once

#include "gbuffer.h"
#include <string>

using namespace std;

namespace aux {

class ReadWriteBuffer : public GrowingBuffer // system data buffer with capability to read from/write to
{
protected:
    unsigned int read_pos; // curr position in buffer for reading
    unsigned int write_pos; // curr position in buffer for writing
    
public:    	
    ReadWriteBuffer() : read_pos(0), write_pos(0) {}
    ReadWriteBuffer(unsigned int sz) : GrowingBuffer(sz), read_pos(0), write_pos(0) {}
	~ReadWriteBuffer() {}
    
    void SetReadPos(unsigned int r_pos) {read_pos = r_pos;}
    void SetWritePos(unsigned int w_pos) {write_pos = w_pos;}

	unsigned int ReadPos() const {return read_pos;}
	unsigned int WritePos() const {return write_pos;}

	int Write(void* data, unsigned int data_sz); // write data to buf	
	template<class T> int WriteScalar(const T &dt);

	int Read(void* data, unsigned int data_sz); // read data from buf
	template<class T> int ReadScalar(T &dt);
};

template<class T>
int ReadWriteBuffer::WriteScalar(const T &dt) // write data to buf
{
	if(write_pos + sizeof(T) > buf_size) // isn't enough space in buffer
	{
		GrowBy(2); // grow to larger size
	}
	*(T*)((char*)buf + write_pos) = dt;
	write_pos += sizeof(T);
return 0;
}

template<class T>
int ReadWriteBuffer::ReadScalar(T &dt) // read data from buf
{
	if(read_pos + sizeof(T) > buf_size) // error - trying to read outside of buffer
	{
		return -1;
	}
	dt = *(T*)((char*)buf + read_pos);	
	read_pos += sizeof(T);
return 0;
}

///////////////////////// RW Operators ///////////////////////////////////
ReadWriteBuffer& operator << (ReadWriteBuffer &b, int val);
ReadWriteBuffer& operator << (ReadWriteBuffer &b, unsigned int val);
ReadWriteBuffer& operator << (ReadWriteBuffer &b, float val); 	
ReadWriteBuffer& operator << (ReadWriteBuffer &b, double val); 
ReadWriteBuffer& operator << (ReadWriteBuffer &b, char val);	
ReadWriteBuffer& operator << (ReadWriteBuffer &b, long val); 	
ReadWriteBuffer& operator << (ReadWriteBuffer &b, unsigned long val);
ReadWriteBuffer& operator << (ReadWriteBuffer &b, bool val); 
ReadWriteBuffer& operator << (ReadWriteBuffer &b, const string &val); 	

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, int &val);	
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, unsigned int &val);	
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, float &val);	
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, double &val); 
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, char &val); 
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, long &val); 	
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, unsigned long &val); 
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, bool &val); 	
ReadWriteBuffer& operator >> (ReadWriteBuffer &b, string &val);
	
}
