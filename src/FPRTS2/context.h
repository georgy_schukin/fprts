#pragma once

#include "migratable.h"
#include <vector>
#include <map>

using namespace std;

namespace fprts
{

class DataFragment;

class InputContext // context for cf input
{
public:
	//vector<DataFragment*> input, output; // input/output data
	vector<DataFragment*> args;
	vector<int> params; // params values
	unsigned int input_sz;

public:
	InputContext() : input_sz(0) {}
	~InputContext() {}
};

/*class ExecContext // context for struct cf execution
{
public:
	vector<DataFragment*> local; // local data
	vector<int> local_p;

public:
	ExecContext() {}
	~ExecContext() {}
};*/

class InputArgContext : public Migratable
{
public:
	class Arg 
	{
	public:
		FragmentId id;
		bool local;
	public:
		Arg() : id(0), local(true) {}
		Arg(FragmentId i, bool loc) : id(i), local(loc) {}
		~Arg() {}
	};
public:
	vector<Arg> args; // input and output	
	vector<int> params; // fact values of params
	unsigned int input_sz;

protected:
	void Pack(Message &msg) const;
	void Unpack(Message &msg);
public:
	InputArgContext() {}
	~InputArgContext() {}
};

}
