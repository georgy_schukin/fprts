#pragma once

#include "message.h"

#include <boost/shared_ptr.hpp>
#include <vector>

namespace fprts {

class MessageStorage // storage for messages
{
protected:
	typedef pair<boost::shared_ptr<Message>, bool> MessagePair;
	std::vector<MessagePair> messages; // allocated messages

public:
	MessageStorage() {}
	~MessageStorage() {}

	int Allocate(unsigned int num, unsigned int msg_sz); // allocate pool of messages
	void Clear();

	Message* Get(); // alloc free message
	Message* Get(unsigned int id); // get msg by id
	int Free(Message* msg); // free message
	int Free(unsigned int id); // free message
};

}
