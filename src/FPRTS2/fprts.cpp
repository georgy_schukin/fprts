#include "fprts.h"

namespace fprts {

int FPRTS::Init(Topology::Type tp) {
	comm_system.SetExecSystem(&exec_system);

	exec_system.SetCommSystem(&comm_system);
	exec_system.SetCodeLibrary(&code_lib);
	//exec_system.SetScheduler(&sched);
	//exec_system.SetDistributor(&distr);

	comm_system.Init(tp); // init communicator
	exec_system.Init();
return 0;
}

int FPRTS::Run(int t_num) {
	comm_system.Start(); // start comm threads
	exec_system.Start(t_num); // start exec threads
return 0;
}

int FPRTS::Shutdown() {
	exec_system.Stop(); // wait for stop
	comm_system.Stop(); // stop comm	
return 0;
}

int FPRTS::Finalize() {
	comm_system.Finalize();
	exec_system.Finalyze();
return 0;
}

int FPRTS::LoadProgram(NodeId node, FragmentId code_id, vector<int> &params) { // load main program for exec
	if(node == exec_system.ThisNode()) {
		FragmentProcedure *proc = exec_system.GetNewProc(); // add proc
			proc->ic.params = params;
		exec_system.ExecProcedure(proc, (CodeGraph*)code_lib.GetCode(code_id));
	}
return 0;
}

/*

struct CopyStruct
{
	DataFragment* df;
	FragmentId cf_id;
	//AccessType ac_type;

	CopyStruct() {}
	CopyStruct(DataFragment* d, const FragmentId& id) : df(d), cf_id(id) {}
};

int FPRTS::SendDFLocal(DataFragment* df, const IdSet& dest_cfs) // send df to local cfs
{	
	if(dest_cfs.size() == 1) // single cf - use simple send
	{
		InputDataFragment(df, cfs[*dest_cfs.begin()]);
		return 0;
	}

	Map<AccessType, IdSet> send_map;
	Map<FragmentId, CopyStruct> copy_map;
	vector<CopyStruct> copy;

	for(IdSet::const_iterator it = dest_cfs.begin();it!= dest_cfs.end();it++) // form map of send
	{
		send_map[cfs[*it]->access_type[df->GetDescr()->GetId()]].insert(*it);
	}

	FragmentId old_cf;
	bool keep_old = false;
	if(!send_map[RW].empty())
	{
		old_cf = *send_map[RW].begin(); 
		send_map[RW].erase(send_map[RW].begin());
		keep_old = true;
	}
	else if(!send_map[WRITE].empty())
	{
		old_cf = *send_map[WRITE].begin(); 
		send_map[WRITE].erase(send_map[WRITE].begin());
		keep_old = true;
	}	

	for(IdSet::const_iterator it = send_map[READ].begin();it!= send_map[READ].end();it++) // send for read
	{
		copy.push_back(CopyStruct(df_multiply_enable ? CopyDF(df, false) : df, *it));	
	}
	for(IdSet::const_iterator it = send_map[WRITE].begin();it!= send_map[WRITE].end();it++) // send for write
	{
		copy.push_back(CopyStruct(df_multiply_enable ? CopyDF(df, false) : df, *it));		
	}
	for(IdSet::const_iterator it = send_map[RW].begin();it!= send_map[RW].end();it++) // send for write
	{
		copy.push_back(CopyStruct(df_multiply_enable ? CopyDF(df, true) : df, *it));
	}
	if(keep_old) copy.push_back(CopyStruct(df, old_cf));

	for(vector<CopyStruct>::iterator it = copy.begin();it != copy.end();it++)
	{
		if(df != (*it).df)
		{
			//cfs[(*it).cf_id]->SubstDF(df->GetDescr()->GetId(), (*it).df->GetDescr()->GetId());
			SubstDF(cfs[(*it).cf_id], df->GetDescr()->GetId(), (*it).df->GetDescr()->GetId());
		}
		InputDataFragment((*it).df, cfs[(*it).cf_id]);
	}
return 0;
}*/

/*int FPRTS::SendDFComm(DataFragmentDescr* dfd, const Map<NodeId, IdSet>& dest_cfs) // send df to nolocal cfs
{
	Message* msg = new Message(Message::DATA_FRAGMENT_SEND, 32);
		//msg->Save();
		msg << dfd->GetId() << dfd->TypeId();
		//msg->Write<FragmentId>(df->GetDescr()->GetId());
		//msg->Write<FragmentId>(df->type_id);
		dfd->Fragment()->Pack(msg);
		//df->Pack(msg);
		const UInt w_pos = msg->GetWritePos();
	for(Map<NodeId, IdSet>::const_iterator it = dest_cfs.begin();it != dest_cfs.end();it++)
	{	
		msg->SetWritePos(w_pos);
		msg << it->second;
		msg->Write<UInt>(it->second.size());
		for(std::set<FragmentId>::const_iterator it2 = it->second.begin();it2 != it->second.end();it2++) // write dest cfs
		{
			LOG << "SendDF : send df " << df->GetDescr()->GetId() << " to cf " << (*it2) << " on node " << it->first << " via MPI" << endl;
			msg->Write<FragmentId>(*it2);
		}
		
		MyTimer t;
		t.Start();
			communicator->Send(msg->GetBuf(), msg->GetBufSize(), msg->GetTag(), it->first);				
		t.Stop();
		if(profile_enable)
		{			
			profiler->AddEvent(new TimeEvent(DF_RECV, t.Begin(), t.End()));
		}
	}
	delete msg;			
return 0;
}

int FPRTS::SendDF(DataFragment* df, const IdSet& dest_cfs)
{
	IdSet cfs_local; // cfs on curr node
	Map<NodeId, IdSet> cfs_nolocal; // cfs on other nodes	

	NodeId node = communicator->GetNode();

	for(IdSet::const_iterator it = dest_cfs.begin();it != dest_cfs.end();it++)
	{
		NodeId dest = cf_locations.Get(*it); // id of dest node, where cf is located		
		if(dest != node) 
		{	
			cfs_nolocal[dest].insert(*it);
		}
		else
		{	
			cfs_local.insert(*it);		
		}
	}

	if(!cfs_local.empty()) SendDFLocal(df, cfs_local);
	if(!cfs_nolocal.empty()) SendDFComm(df, cfs_nolocal);
return 0;
}

int FPRTS::SendDF(DataFragment* df, const FragmentId& cf_id)
{		
	NodeId dest = cf_locations.Get(cf_id); // id of dest node, where cf is located		
	if(dest != communicator->GetNode()) 
	{	
		Message* msg = new Message(Message::DATA_FRAGMENT_SEND, df->GetSize() + Message::GetHeaderSize() + 10*sizeof(UInt));
			msg->Save();
			msg->Write<FragmentId>(df->GetDescr()->GetId());
			msg->Write<FragmentId>(df->type_id);
				df->Pack(msg);
			msg->Write<UInt>(1);
			msg->Write<FragmentId>(cf_id);
			LOG << "SendDF : send df " << df->GetDescr()->GetId() << " to cf " << cf_id << " on node " << dest << " via MPI" << endl;
			communicator->Send(msg->GetBuf(), msg->GetBufSize(), msg->GetTag(), dest);
		delete msg;						
	}
	else
	{
		LOG << "SendDF : send df " << df->GetDescr()->GetId() << " to cf " << cf_id << endl;
		InputDataFragment(df, cfs[cf_id]);
	}
return 0;
}*/

/*int FPRTS::Init(FragmentProgram* fp) // add program to execute
{
	fp->BuildGraph(); // build special info in fragment program

	dfs.insert(fp->dfs.begin(), fp->dfs.end()); // add data fragments to system (TEMP!!! - change id when inserting)
	cfs.insert(fp->cfs.begin(), fp->cfs.end()); // add code fragments to system (TEMP!!!)
	init_send.insert(fp->init_send.begin(), fp->init_send.end()); // TEMP!!!
	code_frags.insert(fp->code_frags.begin(), fp->code_frags.end()); // TEMP !!!
	df_spawners.insert(fp->df_spawners.begin(), fp->df_spawners.end()); // TEMP!!!

	DistributionPlan df_plan, cf_plan;
	

	IdSet fp_dfs, fp_cfs;

	fp->dfs.GetKeys(fp_dfs);
	fp->cfs.GetKeys(fp_cfs);

	distributor->Distribute(this, communicator->GetSize(), fp_cfs, cf_plan); // compute distribution
	distributor->DistributeDFByCF(this, fp_dfs, cf_plan, df_plan);
	distributor->DistributeAll(this, communicator->GetSize(), cf_plan, df_plan);
	

	SpawnCF(cf_plan);
	SpawnDF(df_plan);
		
	//cout << "spawn " << cfs.size() << " " << dfs.size() << " " << cfds.size() << " " << dfds.size() << endl;

	id_step = dfs.size()*(2*communicator->GetNode() + 1); // NEED BETTER ALG !!!

	communicator->Barrier();	
return 0;
}

int FPRTS::Run(const UInt& ex_num, const UInt& s_num) // run system
{	
	exec_system->Run(ex_num);

	exec_threads.resize(ex_num);
	send_threads.resize(s_num); 
	recv_threads.resize(s_num);
	balance_threads.resize(1);

	is_working.Set(true);
	is_done.Set(false);

	t_info.resize(ex_num + s_num + 1);

	executor->Init(ex_num); // init executor
	collector.Init(ex_num);
    
    for(UInt i = 0;i < exec_threads.size();i++) // start exec threads
    {		
		t_info[i].sys = this;
		t_info[i].thread_id = i;
		exec_threads[i].Create(&TExecFunc, (void*)&(t_info[i]));
    }

	if(communicator->GetSize() > 1)
	{
		for(UInt i = 0;i < recv_threads.size();i++) // start send/recv threads
		{
			t_info[ex_num + i].sys = this;	
			t_info[ex_num + i].thread_id = i;	    
			//send_threads[i].Create(&TSendFunc, (void*)&(t_info[ex_num + i]));
			recv_threads[i].Create(&TRecvFunc, (void*)&(t_info[ex_num + i]));	
		}
	}
	t_info[ex_num + s_num].sys = this;
	t_info[ex_num + s_num].thread_id = 0;
	//balance_threads[0].Create(&TBalanceFunc, (void*)&(t_info[ex_num + s_num]));	

	CheckFreeCF();
	SendInitDF();

return 0;
}*/

/*
void FPRTS::BalanceFunc(const UInt& t_id) // function for send thread
{   
	list<pair<double, UInt> > stat_data;

    while(IsWorking())
    {
		stat_data.push_back(pair<double, UInt>(MyTimer::GetCurrTime(), collector.GetCurrTotalLoad()));
		//usleep(500);	
	}   

	ofstream f_out;	

	char file[50];
	sprintf(file, "stat_%d.txt", communicator->GetNode());

	f_out.open(file);
	for(list<pair<double, UInt> >::const_iterator it = stat_data.begin();it != stat_data.end();it++)
	{		
		f_out << scientific << it->first << " " << it->second << endl;
	}
	f_out.close();
}

int FPRTS::OnDataFragment(Message *msg)
{
	FragmentId df_id = msg->Read<FragmentId>(); // read id
	FragmentId df_type = msg->Read<FragmentId>(); // read type
	DataFragment* df = 0;
	if(dfs.Exists(df_id)) // there is a df info on the node
	{
		df = dfs.Get(df_id); // get a info
	}
	else
	{
		df = df_spawners.Get(df_type)->Spawn(); // spawn a new data fragment of needed type
		dfs.Add(df_id, df); // add to info
	}	
	df->Unpack(msg);
	AddDataFragment(df_id, df);
return 0;
}

int FPRTS::OnCompFragment(Message *msg)
{
	FragmentId cf_id = msg->Read<FragmentId>(); // read id
	CompFragment* cf = cfs[cf_id];	
	AddCompFragment(cf_id, cf);
return 0;
}

int FPRTS::OnDataFragmentSend(Message *msg)
{
	FragmentId df_id = msg->Read<FragmentId>(); // read id
	FragmentId df_type = msg->Read<FragmentId>(); // read type
	DataFragment* df = 0;
	if(dfs.Exists(df_id)) // there is a df info on the node
	{
		df = dfs.Get(df_id); // get a info
	}
	else
	{
		df = df_spawners.Get(df_type)->Spawn(); // spawn a new data fragment of needed type
		dfs.Add(df_id, df); // add to info
	}	
	df->Unpack(msg);
	AddDataFragment(df_id, df);
	UInt cnt = msg->Read<UInt>();

	set<FragmentId> dest_cfs;
	for(UInt i = 0;i < cnt;i++) dest_cfs.insert(msg->Read<FragmentId>());
	SendDFLocal(df, dest_cfs);
return 0;
}*/

}
