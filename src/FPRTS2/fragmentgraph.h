#pragma once

#include "datafragment.h"
#include "compfragment.h"
#include "migratable.h"
#include "codegraph.h"
#include "distributor.h"

namespace fprts {

class FragmentGraph // graph of procedure to exec
{
public:
	class DFVertex : public Migratable
	{
	public:
		FragmentId id;
		DataFragment::Type type;
		vector<int> params;
	protected:
		virtual void Pack(Message& msg) const; // pack itself to buffer
		virtual void Unpack(Message& msg); // unpack from buffer
	public:
		DFVertex() {}
		~DFVertex() {}
	};

	class CFVertex : public Migratable
	{
	public:
		FragmentId id;
		FragmentId code_id;
		unsigned int dep_cnt;
		InputArgContext iac;		
		FragmentIdSet next;
	protected:
		virtual void Pack(Message& msg) const; // pack itself to buffer
		virtual void Unpack(Message& msg); // unpack from buffer
	public:
		CFVertex() : dep_cnt(0) {}
		~CFVertex() {}
	};

public:
	typedef map<FragmentId, FragmentGraph::DFVertex> DFMap;
	typedef map<FragmentId, FragmentGraph::CFVertex> CFMap;
	typedef map<VertexId, FragmentId> VToFMap;

public:
	FragmentGraph::DFMap dfs; // data fragments
	FragmentGraph::CFMap cfs; // comp fragments	

protected:
	FragmentGraph::VToFMap v_to_f;
	FragmentId df_id_cnt, cf_id_cnt;

protected:
/*	virtual Message& Pack(Message& msg); // pack itself to buffer
	virtual Message& Unpack(Message& msg); // unpack from buffer*/

	void BuildDFVertex(const VertexData &v, FragmentId id, FragmentGraph::DFVertex &dv);
	void BuildCFVertex(const VertexCall &v, FragmentId id, const InputContext &ic, FragmentGraph::CFVertex &cv);
	InputArgContext::Arg ResolveArg(const VertexCall::Arg &arg, const InputContext &ic);

public:
	FragmentGraph() : df_id_cnt(0), cf_id_cnt(0) {}
	~FragmentGraph() {}

	void Build(CodeGraph *code, const InputContext &ic); // build from code graph
	void Intersect(NodeId node, const Distributor::DistributionPlan &plan); // remove some vertices from graph
};

}
