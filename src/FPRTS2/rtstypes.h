#pragma once

#include <map>
#include <vector>
#include <queue>
#include <string>
#include <list>
#include <set>
#include <queue>

using namespace std;

namespace fprts {

typedef unsigned int FragmentId; // type of fragments id in RTS
typedef int NodeId; // type of node id in RTS
typedef unsigned int VertexId;

//typedef pair<int, int> Range;

typedef set<FragmentId> FragmentIdSet;

typedef pair<NodeId, FragmentId> ProcedureId;

}
