#include "test.h"
#include <boost/assign/std/vector.hpp>

using namespace boost::assign;

namespace fptest {

void CodeInit::Init(CodeLibrary &lib) {
	output.push_back("num");
}

void CodeInit::Exec(const InputContext &ic) {
	int &out = GetOutput<int>(ic, 0);
	out = 7;
	printf("Exec 'init' : res = %d\n", out);
	boost::this_thread::sleep(boost::posix_time::millisec(5));
}

void CodeCount::Init(CodeLibrary &lib) {
	input.push_back("num");
	output.push_back("num");
}

void CodeCount::Exec(const InputContext &ic) {
	int &in = GetInput<int>(ic, 0);
	int &out = GetOutput<int>(ic, 0);
	out = in + 5;
	if(out != 12) {
		printf("Strange!\n");
	}
	printf("Exec 'count' : res = %d\n", out);
	boost::this_thread::sleep(boost::posix_time::millisec(5));
}

void CodePrint::Init(CodeLibrary &lib) {
	input.push_back("num");			
}

void CodePrint::Exec(const InputContext &ic) {
	int &res = GetInput<int>(ic, 0);
	printf("Exec 'print' : res = %d\n", res);
	boost::this_thread::sleep(boost::posix_time::millisec(5));
}

void Test1::Init(CodeLibrary &lib) {				
	lib.Register<CodeInit>("Init");
	lib.Register<CodeCount>("Count");
	lib.Register<CodePrint>("Print");
}

void Test1::Build(const CodeLibrary &lib) {
	typedef VertexCall::Arg Arg;		

	for(int i = 0;i < 500;i++) {		
		VertexId v1 = AddDataVertex(DataFragment::BLOCK, vector<int>(1, sizeof(int)));

		vector<Arg> arg1, arg2, arg3;
			arg1 += Arg(Arg::LOCAL, v1);
			arg2 += Arg(Arg::LOCAL, v1), Arg(Arg::LOCAL, v1);
			arg3 += Arg(Arg::LOCAL, v1);

		VertexId v2 = AddCallVertex(lib.GetId("Init"), arg1, 0, 1);
		VertexId v3 = AddCallVertex(lib.GetId("Count"), arg2, 1, 1);
		VertexId v4 = AddCallVertex(lib.GetId("Print"), arg3, 1, 0);

		AddArc(v2, v3);
		AddArc(v3, v4);
	}
}

}