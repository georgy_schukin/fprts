#include "fragment.h"

namespace fprts {

void Fragment::Pack(Message &msg) const { // pack function
	msg << id;
}

void Fragment::Unpack(Message &msg) { // unpack function
	msg >> id;
}

}