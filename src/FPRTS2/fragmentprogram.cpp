#include "fragmentprogram.h"

namespace rts
{
/*void FragmentProgram::Intersection(const set<SystemId>& s1, const set<SystemId>& s2, set<SystemId>& res)
{
	for(set<SystemId>::const_iterator it = s1.begin();it != s1.end();it++) 
		if(s2.find(*it) != s2.end()) res.insert(*it);
}

void FragmentProgram::Difference(const set<SystemId>& s1, const set<SystemId>& s2, set<SystemId>& res)
{
	for(set<SystemId>::const_iterator it = s1.begin();it != s1.end();it++) 
		if(s2.find(*it) == s2.end()) res.insert(*it);
}

void FragmentProgram::Remove(const SystemId& cf_id, set<SystemId>& sended)
{
	CompFragment* root = cfs[cf_id];
	for(set<SystemId>::iterator it = root->dfs.begin();it != root->dfs.end();it++) // for all dfs of cf
	{
		if(init_send.Exists(*it) && (sended.find(*it) != sended.end()))
		{
			init_send[*it].erase(cf_id);
		}
	}						
	for(set<SystemId>::iterator it = root->connected_cfs.begin();it != root->connected_cfs.end();it++) // for all cfs incoming from prev
	{	
		set<SystemId> snd = sended;
		for(Map<SystemId, set<SystemId> >::iterator it2 = root->output_connection.begin();it2 != root->output_connection.end();it2++) // for all cfs incoming from prev
		{
			for(set<SystemId>::iterator it3 = it2->second.begin();it3 != it2->second.end();it3++) // for all cfs incoming from prev
			{
				if(*it3 == *it) snd.insert(it2->first);
				Remove(*it, snd);
			}
		}
	}
}

void FragmentProgram::BuildPath(const SystemId& cf_id, Map<SystemId, SystemId>& last_df_owner)
{
	CompFragment* root = cfs[cf_id];
	for(set<SystemId>::iterator it = root->dfs.begin();it != root->dfs.end();it++) // for all dfs of cf
	{
		if(!last_df_owner.Exists(*it)) // df was not sended yet
		{			
			init_send[*it].insert(cf_id); // add to init send										
		}
		else // there is an owner for data
		{				
			cfs[last_df_owner.Get(*it)]->output_connection[*it].insert(cf_id); // make send connection					
			cfs[last_df_owner.Get(*it)]->connected_cfs.insert(cf_id);			
			cfs[last_df_owner.Get(*it)]->edges.insert(cf_id);
			cfs[cf_id]->edges.insert(last_df_owner.Get(*it));
		}									
	}	
	if(cf_order_reverse.Exists(cf_id)) // there is a children for root
	{		
		Map<SystemId, SystemId> last = last_df_owner;
		for(set<SystemId>::iterator it = root->dfs.begin();it != root->dfs.end();it++) // for all dfs of cf
		{
			if(root->access_type[*it] > READ) last[*it] = cf_id; // change owner if df is written
		}	
		for(set<SystemId>::iterator it = cf_order_reverse[cf_id].begin();it != cf_order_reverse[cf_id].end();it++) // for all cfs incoming from prev
		{											
			BuildPath(*it, last);
		}
	}
}*/

void SortByAccess(BaseFragmentProgram* fp, const IdSet& cfs, const SystemId& df_id, list<SystemId>& res)
{
	for(IdSet::const_iterator it = cfs.begin();it != cfs.end();it++) if(fp->cfs[*it]->access_type[df_id] == READ) res.push_back(*it);
	for(IdSet::const_iterator it = cfs.begin();it != cfs.end();it++) if(fp->cfs[*it]->access_type[df_id] == WRITE) res.push_back(*it);
	for(IdSet::const_iterator it = cfs.begin();it != cfs.end();it++) if(fp->cfs[*it]->access_type[df_id] == RW) res.push_back(*it);
}

void FragmentProgram::GetFreeCF(IdSet& free_cf) // get all independent comp fragments
{
	for(CFCollection::const_iterator it = cfs.begin();it != cfs.end();it++) // for each cf
	{
		if(!cf_order.Exists(it->first)) // no order for cf
		{
			free_cf.insert(it->first); // add to set
		}		
	}
}

void FragmentProgram::GetNextCF(const IdSet& cf_layer, IdSet& cf_next_layer) // get next layer of cfs
{
	for(IdSet::const_iterator it = cf_layer.begin();it != cf_layer.end();it++)
	{
		CompFragment* cf = cfs[*it];
		for(IdSet::const_iterator it2 = cf->connected_cfs.begin();it2 != cf->connected_cfs.end();it2++) // use connections to find next layer
		{
			cf_next_layer.insert(*it2);
		}
	}
}

void FragmentProgram::BuildConnections(const IdSet& start) // build data connections between comp fragments
{
	IdSet cfs_id = start; // start set
	Map<SystemId, SystemId> last_df_owner;
	while(!cfs_id.empty()) // whine no cfs left
	{
		for(IdSet::const_iterator it = cfs_id.begin();it != cfs_id.end();it++) // for each cf in set
		{
			CompFragment* cf = cfs[*it];
			for(IdSet::iterator it2 = cf->dfs.begin();it2 != cf->dfs.end();it2++) // for all dfs of cf
			{
				if(!last_df_owner.Exists(*it2)) // df was not sended yet
				{			
					init_send[*it2].insert(*it); // add to init send										
				}
				else // there is an owner for data
				{	
					CompFragment* prev = cfs[last_df_owner.Get(*it2)];
					CompFragment* next = cfs[*it];
						prev->output_connection[*it2].insert(*it); // make send connection					
						prev->connected_cfs.insert(*it);	
						prev->edges.insert(*it);
						next->edges.insert(last_df_owner.Get(*it2));
						next->order_weight = prev->order_weight - 10;
				}
			}
		}	

		IdSet next;
		//Map<SystemId, SystemId> last = last_df_owner;

		for(IdSet::iterator it = cfs_id.begin();it != cfs_id.end();it++) // for each cf in set
		{
			CompFragment* cf = cfs[*it];
			if(cf_order_reverse.Exists(*it)) // there is a children for cf
			{				
				for(IdSet::iterator it2 = cf->dfs.begin();it2 != cf->dfs.end();it2++) // for all dfs of cf
				{
					if(cf->access_type[*it2] > READ) last_df_owner[*it2] = *it; // change owner if df is written
				}	
				for(IdSet::iterator it2 = cf_order_reverse[*it].begin();it2 != cf_order_reverse[*it].end();it2++) // for all cfs incoming from prev
				{
					next.insert(*it2);
				}			
			}
		}
		cfs_id = next; // change to the nett set
		//last_df_owner = last;
	}	
}

void FragmentProgram::BuildGraph()
{
	for(CFCollection::iterator it = cfs.begin();it != cfs.end();it++) // init param info for comp fragment
	{
		CompFragment* cf = it->second;
		for(Map<UInt, Param>::iterator it2 = cf->input_dfs.begin();it2 != cf->input_dfs.end();it2++)
		{
			cf->dfs.insert(it2->second.df_id);
			cf->access_type[it2->second.df_id] = cf->GetAccessType(it2->second.df_id);
		}						
	}

	IdSet free_cf; // cf without dependencies	
	GetFreeCF(free_cf);
	
	for(IdSet::iterator it = free_cf.begin();it != free_cf.end();it++) 
	{		
		cfs[(*it)]->order_weight = cfs.size()*10;	
	}	

	BuildConnections(free_cf);
}

void FragmentProgram::OptimizeDF(DataFragment* df)
{	
	SystemId df_id = df->GetDescr()->GetId();

	vector<pair<SystemId, AccessType> > df_path;
	set<SystemId> layer = init_send[df_id];

	while(!layer.empty())
	{
		set<SystemId> new_layer;
		for(set<SystemId>::const_iterator it = layer.begin();it != layer.end();it++)
		{
			df_path.push_back(pair<SystemId, AccessType>((*it), cfs[*it]->access_type[df_id]));
		}
		for(set<SystemId>::const_iterator it = layer.begin();it != layer.end();it++)
		{
			CompFragment* cf = cfs[*it];
			if(cf->output_connection.Exists(df_id))
			{
				new_layer.insert(cf->output_connection[df_id].begin(), cf->output_connection[df_id].end());
			}
		}
		layer = new_layer;
	}
}

void FragmentProgram::Optimize() // optimize fragment program
{
	for(DFCollection::iterator it = dfs.begin();it != dfs.end();it++) // init param info for comp fragment
	{
		OptimizeDF(it->second);
	}
}

int FragmentProgram::AddDF(const SystemId& df_id, DataFragment* df, const SystemId& type_id)
{
	df->type_id = type_id;
	dfs.Add(df_id, df);
return 0;
}

int FragmentProgram::AddCF(const SystemId& cf_id, const SystemId& code_id, const UInt& p_num, Param* params, const UInt& weight, const UInt& prior)
{
	CompFragment* cf = new CompFragment(cf_id);

	for(UInt i = 0;i < p_num;i++)
	{
		cf->input_dfs[i] = params[i];
	}	
	cf->code_id = code_id;
	cf->weight = weight;
	cf->priority = prior;
	cfs.Add(cf_id, cf);
return 0;
}

void FragmentProgram::SetOrder(const SystemId& cf_id_first, const SystemId& cf_id_second)
{
	cf_order[cf_id_second].insert(cf_id_first); // do A before B
	cf_order_reverse[cf_id_first].insert(cf_id_second); // do B after A
}

}