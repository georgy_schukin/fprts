#include "rtstypes.h"
#include "datafragment.h"
#include "compfragment.h"

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <map>

using namespace std;

namespace fprts {

class FragmentStorage
{
public:
	typedef map<FragmentId, boost::shared_ptr<DataFragment> > DFMap;
	typedef map<FragmentId, boost::shared_ptr<CompFragment> > CFMap;
protected:
	friend class FragmentProcedure;
	DFMap dfs; // data fragments storage
	CFMap cfs; // comp fragments storage	
	//FragmentId id_counter;
public:
	FragmentStorage() {}
	~FragmentStorage() {}

	void AddDF(DataFragment *df);
	void AddCF(CompFragment *cf);

	DataFragment* GetDF(FragmentId id);
	CompFragment* GetCF(FragmentId id);

	void RemoveDF(FragmentId id);
	void RemoveCF(FragmentId id);

	void RemoveDFs(const FragmentIdSet &ids);
	void RemoveCFs(const FragmentIdSet &ids);

	bool ExistsDF(FragmentId id);
	bool ExistsCF(FragmentId id);

	//FragmentId GetNextId();
	//FragmentId GetNextIds(unsigned int num);

	//const FragmentStorage::DFMap& GetDFs() const {return dfs;}
	//const FragmentStorage::CFMap& GetCFs() const {return cfs;}
};

}