#include "rwbuffer.h"

namespace aux
{

//////////////////////////// Read Write Buffer /////////////////////////////////////
int ReadWriteBuffer::Write(void* data, unsigned int data_sz) { // write data to buf
	if(write_pos + data_sz > buf_size) { // isn't enough space in buffer
		GrowBy(2); // grow to larger size
	}
	memcpy((void*)((char*)buf + write_pos), data, data_sz);
	write_pos += data_sz;
return 0;
}
    
int ReadWriteBuffer::Read(void* data, unsigned int data_sz) { // read data from buf
	if(read_pos + data_sz > buf_size) { // error - trying to read outside of buffer
		return -1;
	}
	memcpy(data, (void*)((char*)buf + read_pos), data_sz);
	read_pos += data_sz;
return 0;
}

//////////////////////////// RW Operators /////////////////////////////////
ReadWriteBuffer& operator << (ReadWriteBuffer &b, int val) {
	b.WriteScalar<int>(val);
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, unsigned int val)  {
	b.WriteScalar<unsigned int>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, float val) {
	b.WriteScalar<float>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, double val) {
	b.WriteScalar<double>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, char val) {
	b.WriteScalar<char>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, long val) {
	b.WriteScalar<long>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, unsigned long val) {
	b.WriteScalar<unsigned long>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, bool val) {
	b.WriteScalar<bool>(val); 
return b;
}

ReadWriteBuffer& operator << (ReadWriteBuffer &b, const std::string &val) {	
	unsigned int sz = val.size();
	b << sz;
	b.Write((void*)val.c_str(), val.size()*sizeof(char)); 
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, int &val) {
	b.ReadScalar<int>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, unsigned int &val) {
	b.ReadScalar<unsigned int>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, float &val) {
	b.ReadScalar<float>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, double &val) {
	b.ReadScalar<double>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, char &val) {
	b.ReadScalar<char>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, long &val) {
	b.ReadScalar<long>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, unsigned long &val) {
	b.ReadScalar<unsigned long>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, bool &val) {
	b.ReadScalar<bool>(val);
return b;
}

ReadWriteBuffer& operator >> (ReadWriteBuffer &b, std::string &val) {
	unsigned int sz;
	b >> sz;

	if(b.ReadPos() + sz*sizeof(char) <= b.BufSize()) {
		val = string((char*)b.Buf() + b.ReadPos(), sz);
		b.SetReadPos(b.ReadPos() + sz*sizeof(char));		
	} else { // truncate
		val = string((char*)b.Buf() + b.ReadPos(), (b.BufSize() - b.ReadPos())/sizeof(char));
		b.SetReadPos(b.BufSize());
	}
	return b;
}

}