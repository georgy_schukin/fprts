#pragma once

#include "fragment.h"
#include "rwbuffer.h"
#include "rtstypes.h"
#include "context.h"

namespace fprts {

class CompFragment : public Fragment // comp fragment - call of function with args
{
public:
	enum State // states of cf
	{
		NEW = 0,	// added to proc
		FREE,		// deps are satisfied
		READY,		// ready to exec
		SCHED,		// ready and taken by scheduler
		EXEC,		// executing
		DONE		// finished execution
	};

public:	
	FragmentId code_id; // id of code fragment
	//InputArgContext iac; // input args for code fragment
	InputContext ic; // real input (need before exec)
	//State state;
	//unsigned int dep_cnt;
	//FragmentIdSet arcs_out;

protected:
	virtual void Pack(Message &msg); // pack function
	virtual void Unpack(Message &msg); // unpack function

public:
	CompFragment(FragmentId i) : Fragment(i) {}
	CompFragment(FragmentId i, FragmentId code) : Fragment(i), code_id(code) {}
	virtual ~CompFragment() {}

	void SetCodeId(FragmentId id) {code_id = id;}
	//void SetState(State s) {state = s;}
};

class CompFragmentGroup : public CompFragment
{
public:
	pair<FragmentId, FragmentId> range;
	unsigned int size;
public:
	CompFragmentGroup(FragmentId i) : CompFragment(i) {}
	CompFragmentGroup(FragmentId i, FragmentId code) : CompFragment(i, code) {}
	virtual ~CompFragmentGroup() {}
};

/*class CFAtom : public CompFragment // atomic comp fragment
{
public:
	CFAtom(FragmentId i) : CompFragment(i) {}	
	CFAtom(FragmentId i, FragmentId c) : CompFragment(i, c) {}
	virtual ~CFAtom() {}

	//virtual bool IsAtom() const {return true;}

	virtual void Pack(ReadWriteBuffer& buf); // pack itself to buffer
	virtual void Unpack(ReadWriteBuffer& buf); // unpack itself from buffer
};

class CFStruct : public CompFragment // call of struct code
{
public:	
	class FragmentProcedure *proc;
	//ProcedureId parent;
	//ExecContext ec;

public:
	CFStruct(FragmentId i) : CompFragment(i) {}	
	CFStruct(FragmentId i, FragmentId c) : CompFragment(i, c) {}
	virtual ~CFStruct() {}
	
	//virtual bool IsAtom() const {return false;}	

	virtual void Pack(ReadWriteBuffer& buf); // pack itself to buffer
	virtual void Unpack(ReadWriteBuffer& buf); // unpack itself from buffer
};*/

/*class CFIndexed : public CFStruct // indexed array of cfs
{
public:
	CompFragment *core; // cf to exec
	vector<CompFragmentIndexedArg> input, output; // args for cfs
};*/

/*class CFBlock : public CompFragment // block of cfs
{
public:
	vector<CompFragment*> elems;

public:
	virtual CFType Type() const {return CFT_BLOCK;}
};

class CFMulti : public CompFragment // multi cf (for cycle)
{
public:
	CompFragment* elem;

public:
	virtual CFType Type() const {return CFT_MULTI;}
};

class CFIter : public CompFragment // iterationable cf (while cycle)
{
public:
	CompFragment* elem;

public:
	virtual CFType Type() const {return CFT_ITER;}
};

class CFCond : public CompFragment // conditional cf (if-else)
{
public:
	CompFragment* elem_if;
	CompFragment* elem_else;

public:
	virtual CFType Type() const {return CFT_COND;}
};*/

}
