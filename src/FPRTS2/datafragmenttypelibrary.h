#pragma once

#include "ptrmap.h"
#include "datafragmenttype.h"

namespace fprts
{

////////////////////////// Data Fragment Type Library ///////////////////////////
class DataFragmentTypeLibrary // library of df types
{
protected:
	PtrMap<FragmentId, DataFragmentType> dfts;
	
public:
	DataFragmentTypeLibrary() {}
	~DataFragmentTypeLibrary() {}

	FragmentId RegisterType(const string& name, DataFragmentType* dft); // reg dft in library

	DataFragmentType* Get(FragmentId dft_id); // get dft by id

	bool Exists(FragmentId dft_id); // chekc if cdf exists
};

}
