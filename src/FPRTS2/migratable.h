#pragma once

#include "message.h"

namespace fprts {

class Migratable // interface for fragment migration
{
protected:
	virtual void Pack(Message& msg) const = 0; // pack itself to buffer
	virtual void Unpack(Message& msg) = 0; // unpack itself from buffer

public:
	friend Message& operator << (Message &msg, const Migratable &obj) {obj.Pack(msg);return msg;} // pack operator for fragment
	friend Message& operator >> (Message &msg, Migratable &obj) {obj.Unpack(msg);return msg;} // unpack operator for fragment
};

}
