#include "fragmentgraph.h"
#include <boost/foreach.hpp>

namespace fprts {

void FragmentGraph::BuildDFVertex(const VertexData &v, FragmentId id, FragmentGraph::DFVertex &dv) {
	dv.id = id;
	dv.type = v.data_type;
	dv.params = v.data_params;
}

void FragmentGraph::BuildCFVertex(const VertexCall &v, FragmentId id, const InputContext &ic, FragmentGraph::CFVertex &cv) {
	cv.id = id;
	cv.code_id = v.code_id;
	cv.iac.input_sz = v.input_sz;
	for(unsigned int i = 0;i < v.input_sz + v.output_sz;i++) { // fill args
		cv.iac.args.push_back(ResolveArg(v.args[i], ic));
	}
}

InputArgContext::Arg FragmentGraph::ResolveArg(const VertexCall::Arg &arg, const InputContext &ic) {
	InputArgContext::Arg res;
	switch(arg.arg_type) {
		case VertexCall::Arg::LOCAL: { // arg is local df
			res.id = v_to_f.find(arg.id)->second; // arg is is data vertex id; convert to df id
			res.local = true; 
			break;
		}
		case VertexCall::Arg::INPUT: { // arg is input
			res.id = ic.args[arg.id]->GetId(); // get id from input context
			res.local = false;
			break;
		}
	}
return res;
}

void FragmentGraph::Build(CodeGraph *code, const InputContext &ic) { // init proc by graph
	BOOST_FOREACH(const CodeGraph::DataVertexMap::value_type &dv, code->data_vertices) { // create df vertices
		FragmentId id = df_id_cnt++;
		v_to_f[dv.first] = id;
		BuildDFVertex(dv.second, id, dfs[id]);
	}
	BOOST_FOREACH(const CodeGraph::CallVertexMap::value_type &cv, code->call_vertices) { // create cf vertices
		FragmentId id = cf_id_cnt++;
		v_to_f[cv.first] = id;
		BuildCFVertex(cv.second, id, ic, cfs[id]);
	} 
	BOOST_FOREACH(const CodeGraph::ArcMap::value_type &ai, code->arcs_in) {	// for each vertex with incoming arcs
		cfs[v_to_f[ai.first]].dep_cnt = ai.second.size(); // set dep cnt for num of incoming arcs
	}
	BOOST_FOREACH(const CodeGraph::ArcMap::value_type &ao, code->arcs_out) { // for each vertex with outcoming arcs
		FragmentIdSet &next = cfs[v_to_f[ao.first]].next;
		BOOST_FOREACH(VertexId cv_id, ao.second) { // create arcs for graph
			next.insert(v_to_f[cv_id]);
		}
	}
	v_to_f.clear(); // don't erase if map will be used later
}

void FragmentGraph::Intersect(NodeId node, const Distributor::DistributionPlan &plan) { // graph was distributed - erase moved dfs/cfs	
	BOOST_FOREACH(const Distributor::DistributionPlan::value_type &dp, plan) {
		if(dp.first != node) {
			BOOST_FOREACH(FragmentId df_id, dp.second.dfs) { // for dfs 
				dfs.erase(df_id);
			}
			BOOST_FOREACH(FragmentId cf_id, dp.second.cfs) { // for cfs 
				cfs.erase(cf_id);
			}
		}
	}
}


void FragmentGraph::DFVertex::Pack(Message &msg) const {	
	msg << id << (int)type;
	msg << params;
}

void FragmentGraph::DFVertex::Unpack(Message &msg) {
	int tp;
	msg >> id >> tp;	
	type = (DataFragment::Type)tp;
	msg >> params;
}

void FragmentGraph::CFVertex::Pack(Message &msg) const {
	msg << id << code_id << dep_cnt;
	msg << iac;
	msg << next;
}

void FragmentGraph::CFVertex::Unpack(Message &msg) {
	msg >> id >> code_id >> dep_cnt;
	msg >> iac;
	msg >> next;
}

}
