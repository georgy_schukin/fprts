#include "fpgraph.h"

namespace rts
{

///////////////////////////////// FP Graph ///////////////////////////////////////////
void FPGraph::ExpandVertex(CompFragmentDescr* root, bool rec = false)
{
	for(DataDescrPtrArray::iterator it = root->dfs.begin();it != root->dfs.end();it++) // check new df
	{		
		available_df.insert((*it)->Id()); // add to available
	}
	for(CompDescrPtrArray::iterator it = root->cfs.begin();it != root->cfs.end();it++) // for all children cf
	{
		if((*it)->CFType() == FUNC) // this is cf call
		{
		}
	}
}

void FPGraph::ExpandVertex(SystemId vertex_id, CompFragmentDescr* root, FPGraph::Context& con, bool rec)
{
	if(!tree->is_expanded) return;

	VertexInfo& v_info = cf_vertexes[vertex_id];

	for(DataDescrPtrArray::iterator it = root->dfs.begin();it != root->dfs.end();it++) // new df 
	{
		df_init[(*it)->Id()] = IdSet();
		con.available_df.insert((*it)->Id());
	}

	for(CompDescrPtrArray::iterator it = root->cfs.begin();it != root->cfs.end();it++) // for all children cf
	{
		SystemId cf_id = (*it)->Id();

		if((*it)->CFType() == SCALAR) // this is cf call
		{
			ScalarCFDescr* cf = (ScalarCFDescr*)(*it);

			for(UInt i = 0;i < cf->input.size();i++) // for each input var
			{
				SystemId df_id = cf->input[i]->Id();
				for(Map<SystemId, IdSet>::iterator it2 = v_info.input_connection.begin();it2 != v_info.input_connection.end();it2++) // for all input connections of root
				{
					if(it2->second.find(df_id) != it2->second.end()) // receive this df from another cf
					{
						cf_vertexes[cf_id].input_connection[it2->first].insert(df_id);
						cf_vertexes[it2->first].output_connection[cf_id].insert(df_id);
					}
					if(cf->access_type[i] > READ) // change df owner
					{
						con.df_owners[df_id] = cf_id;
					}
				}
			}
		}
		for(Map<SystemId, IdSet>::iterator it2 = v_info.input_connection.begin();it2 != v_info.input_connection.end();it2++)
		{
		}	
	}
	for(CompDescrPtrArray::iterator it = root->cfs.begin();it != root->cfs.end();it++) // for all children cf
	{
		//ExpandVertex();
	}

	cf_vertextes.erase(vertex_id); // delete old vertex
}

void FPGraph::GetUsedDF(CompFragmentDescr* root, Map<SystemId, IdSet>& res)
{
	if(root->CFType() == SCALAR)
	{
		ScalarCFDescr* cf = (ScalarCFDescr*)root;
		for(DataDescrPtrArray::iterator it = cf->input.begin();it != cf->input.end();it++)
		{
			res[(*it)->Id()].insert(cf->Id());
		}
	}
	for(CompDescrPtrArray::iterator it = root->cfs.begin()(;it != root->cfs.end();it++)
	{
		GetUsedDF(*it, res);
	}
}

/*void FragmentProgramGraph::GetFreeCF(FragmentScope* scope, IdSet& free_cf); // get all independent comp fragments
{
	for(CFDCollection::const_iterator it = scope->CFds().begin();it != scope->CFds().end();it++) // for each cf
	{
		if(!scope->cf_order.Exists(it->first)) // no order for cf
		{
			free_cf.insert(it->first); // add to set
		}		
	}
}

void FragmentProgramGraph::GetNextCF(const IdSet& cf_layer, IdSet& cf_next_layer) // get next layer of cfs
{
	for(IdSet::const_iterator it = cf_layer.begin();it != cf_layer.end();it++)
	{
		CompFragment* cf = cfs[*it];
		for(IdSet::const_iterator it2 = cf->connected_cfs.begin();it2 != cf->connected_cfs.end();it2++) // use connections to find next layer
		{
			cf_next_layer.insert(*it2);
		}
	}
}

void FragmentProgramGraph::BuildConnections(FragmentScope* scope, const IdSet& start) // build data connections between comp fragments
{
	IdSet cfs_id = start; // start set
	Map<SystemId, SystemId> last_df_owner;

	while(!cfs_id.empty()) // whine no cfs left
	{
		for(IdSet::const_iterator it = cfs_id.begin();it != cfs_id.end();it++) // for each cf in set
		{
			CompFragmentDescr* cf = scope->CFd(*it);
			for(IdSet::iterator it2 = cf->dfs.begin();it2 != cf->dfs.end();it2++) // for all dfs of cf
			{
				if(!last_df_owner.Exists(*it2)) // df was not sended yet
				{			
					init_send[*it2].insert(*it); // add to init send
				}
				else // there is an owner for data
				{	
					CompFragmentDescr* prev = scope->CFd(last_df_owner.Get(*it2));
					CompFragmentDescr* next = scope->CFd(*it);
						prev->output_connection[*it2].insert(*it); // make send connection					
						prev->connected_cfs.insert(*it);	
						prev->edges.insert(*it);
						next->edges.insert(last_df_owner.Get(*it2));
						next->order_weight = prev->order_weight - 10;
				}
			}
		}	

		IdSet next;
		//Map<SystemId, SystemId> last = last_df_owner;

		for(IdSet::iterator it = cfs_id.begin();it != cfs_id.end();it++) // for each cf in set
		{
			CompFragmentDescr* cf = scope->CFd(*it);
			if(cf_order_reverse.Exists(*it)) // there is a children for cf
			{				
				for(IdSet::iterator it2 = cf->dfs.begin();it2 != cf->dfs.end();it2++) // for all dfs of cf
				{
					if(cf->access_type[*it2] > READ) last_df_owner[*it2] = *it; // change owner if df is written
				}	
				for(IdSet::iterator it2 = cf_order_reverse[*it].begin();it2 != cf_order_reverse[*it].end();it2++) // for all cfs incoming from prev
				{
					next.insert(*it2);
				}			
			}
		}
		cfs_id = next; // change to the nett set
		//last_df_owner = last;
	}	
}

void FragmentProgramGraph::BuildGraph(CompFragmentDescr* root, Map<SystemId, SystemId>& df_last_owner)
{
	SystemId cf_id = root->Id();
	if(root->CFType() == SCALAR)
	{
		vertexes.Add(root->Id(), VertexInfo());
	}
	else for(vector<FragmentDescr>::iterator it = begin->cfs.begin();it != end();it++)
	{
		InitVertextes(*it);
	}
}

void FragmentProgramGraph::InitVertexes(CompFragmentDescr* begin)
{
	if(begin->CFType() == SCALAR)
	{
		vertexes.Add(begin->Id(), VertexInfo());
	}
	else for(vector<FragmentDescr>::iterator it = begin->cfs.begin();it != end();it++)
	{
		InitVertextes(*it);
	}
}

void FragmentProgramGraph::BuildGraph(FragmentScope* s)
{
	IdSet free_cf; // cf without dependencies	
	GetFreeCF(free_cf);
	
	/*for(IdSet::iterator it = free_cf.begin();it != free_cf.end();it++) 
	{		
		cfs[(*it)]->order_weight = cfs.size()*10;	
	}

	BuildConnections(free_cf);
}*/

}