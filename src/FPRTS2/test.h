#include "fprts.h"

using namespace fprts;

namespace fptest {

class CodeInit : public CodeAtom
{
public:
	CodeInit() {}
	~CodeInit() {}

	void Init(CodeLibrary &lib);
	void Exec(const InputContext &ic);
};

class CodeCount : public CodeAtom
{
public:
	CodeCount() {}
	~CodeCount() {}

	void Init(CodeLibrary &lib);
	void Exec(const InputContext &ic);
};

class CodePrint : public CodeAtom
{
public:
	CodePrint() {}
	~CodePrint() {}

	void Init(CodeLibrary &lib);
	void Exec(const InputContext &ic);
};

class Test1 : public CodeGraph
{
public:
	Test1() {}
	~Test1() {}

	void Init(CodeLibrary &lib);
	void Build(const CodeLibrary &lib);
};

}
