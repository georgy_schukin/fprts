#include "fragmentstorage.h"

namespace fprts {

void FragmentStorage::AddDF(DataFragment *df) { // add df	
	dfs[df->GetId()] = boost::shared_ptr<DataFragment>(df); // add to storage
}

void FragmentStorage::AddCF(CompFragment *cf) { // insert data fragment into system
	cfs[cf->GetId()] = boost::shared_ptr<CompFragment>(cf); // add to storage
}

DataFragment* FragmentStorage::GetDF(FragmentId id)  {	
	DFMap::const_iterator it;
	return ((it = dfs.find(id)) != dfs.end()) ? it->second.get() : 0;
}

CompFragment* FragmentStorage::GetCF(FragmentId id)  {	
	CFMap::const_iterator it;
	return ((it = cfs.find(id)) != cfs.end()) ? it->second.get() : 0;
}

void FragmentStorage::RemoveDF(FragmentId id) {	
	dfs.erase(id);
}

void FragmentStorage::RemoveCF(FragmentId id) {
	cfs.erase(id);
}

void FragmentStorage::RemoveDFs(const FragmentIdSet &ids) {
	for(FragmentIdSet::const_iterator it = ids.begin();it != ids.end();it++) {
		dfs.erase(*it);
	}
}

void FragmentStorage::RemoveCFs(const FragmentIdSet &ids) {
	for(FragmentIdSet::const_iterator it = ids.begin();it != ids.end();it++) {
		cfs.erase(*it);
	}
}

bool FragmentStorage::ExistsDF(FragmentId id) {
	return (dfs.find(id) != dfs.end());
}

bool FragmentStorage::ExistsCF(FragmentId id) {
	return (cfs.find(id) != cfs.end());
}

/*FragmentId FragmentStorage::GetNextId() {
	return id_counter++;
}

FragmentId FragmentStorage::GetNextIds(unsigned int num) {
	id_counter += num;
	return id_counter - num;
}*/

}
