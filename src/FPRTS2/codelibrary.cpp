#include "codelibrary.h"
#include "idgen.h"

namespace fprts
{

FragmentId CodeLibrary::GetId(const string &name) const {
	return idgen::Id(name);
}

CodeFragment* CodeLibrary::GetCode(FragmentId id)  {
	return cdfs[id].get(); 
}

bool CodeLibrary::Exists(FragmentId id) const {
	return (cdfs.find(id) != cdfs.end());
}

}
