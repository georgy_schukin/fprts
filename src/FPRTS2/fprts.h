#pragma once

#include "rtstypes.h"
#include "execsystem.h"
#include "commsystem.h"
#include "codelibrary.h"
//#include "scheduler.h"

namespace fprts
{

class FPRTS
{
protected:	
	ExecSystem exec_system;
	CommSystem comm_system;
	CodeLibrary code_lib;
	//SimpleScheduler sched;
	//GreedyDistributor distr;

public:
	FPRTS() {}
	~FPRTS() {}

	template<class T>
	FragmentId RegisterCode(const string &name); // reg code for call

	int LoadProgram(NodeId node, FragmentId code_id, vector<int> &params); // load main program for exec

	int Init(Topology::Type tp = Topology::ALL_TO_ALL);
	int Finalize();	

	int Run(int t_num);	
	int Shutdown();
};

template<class T>
FragmentId FPRTS::RegisterCode(const string &name) { // reg code for call
	return code_lib.Register<T>(name);
}

}
