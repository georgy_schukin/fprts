#pragma once

#include "rtstypes.h"
#include "context.h"
//#include "codegraph.h"

#include <vector>

using namespace std;

namespace fprts {

class CodeLibrary;

class CodeFragment // fragment of code
{
protected:
	vector<string> input; // names of formal input args
	vector<string> output; // names of formal output args
	vector<string> params; // names of formal params	
public:
	virtual void Init(CodeLibrary &lib) = 0; // init params, code, etc.

	virtual bool IsAtom() const = 0;

	const vector<string>& Input() const {return input;}
	const vector<string>& Output() const {return output;}
	const vector<string>& Params() const {return params;}
};

class CodeAtom : public CodeFragment // atom code fragment - call of C++ procedure
{
protected:
	template<class T> T& GetInput(const InputContext &ic, unsigned int num) const;
	template<class T> T& GetOutput(const InputContext &ic, unsigned int num) const;
public:
	virtual bool IsAtom() const {return true;}
	virtual void Exec(const InputContext &ic) = 0; // code to exec
};

template<class T> T& CodeAtom::GetInput(const InputContext &ic, unsigned int num) const {
	return *((T*)((DFAtom*)ic.args[num])->Data());
}

template<class T> T& CodeAtom::GetOutput(const InputContext &ic, unsigned int num) const {
	return *((T*)((DFAtom*)ic.args[ic.input_sz + num])->Data());
}

/*class CodeProc : public CodeFragment // complex structured code fragment (procedure)
{
public:	
	CodeGraph code_graph; // code formal representation
public:
	virtual bool IsAtom() const {return false;}		
	virtual void BuildCodeGraph(CodeGraph *graph, const CodeLibrary &lib) = 0; // build code graph

	//const CodeGraph& GetGraph() const {return code;}
};*/

}
