#pragma once

#include "buffer.h"

namespace aux {

class GrowingBuffer : public Buffer // buffer that can grow
{
public:
	GrowingBuffer();
	GrowingBuffer(unsigned int sz);
	GrowingBuffer(const GrowingBuffer &buf);
	~GrowingBuffer();

	void* GrowBy(unsigned int mult = 2);	
	void* GrowOn(unsigned int add_sz);	
	void* GrowTo(unsigned int new_sz);
};

}
