#include "gbuffer.h"

namespace aux
{

GrowingBuffer::GrowingBuffer() {}

GrowingBuffer::GrowingBuffer(unsigned int sz) : Buffer(sz) {}

GrowingBuffer::GrowingBuffer(const GrowingBuffer &buf) : Buffer(buf) {}

GrowingBuffer::~GrowingBuffer() {}

void* GrowingBuffer::GrowBy(unsigned int mult)
{
	return Resize(BufSize()*mult);
}

void* GrowingBuffer::GrowOn(unsigned int add_sz)
{		
	return Resize(BufSize() + add_sz);
}

void* GrowingBuffer::GrowTo(unsigned int new_sz)
{
	return Resize(new_sz);
}

}
