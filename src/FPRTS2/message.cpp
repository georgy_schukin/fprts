#include "message.h"

namespace fprts {

Message& operator << (Message &msg, const ProcedureId &id) {
	msg << id.first << id.second;
//	printf("Write %d %d\n", id.first, id.second);
return msg;
}

Message& operator >> (Message &msg, ProcedureId &id) {
	msg >> id.first >> id.second;
//	printf("Read %d %d\n", id.first, id.second);
return msg;
}

Message& operator << (Message &msg, const FragmentIdSet &ids) {
	unsigned int size = ids.size();
		msg << size;
	for(FragmentIdSet::const_iterator it = ids.begin();it != ids.end();it++) {
		msg << *it;
	}
return msg;
}

Message& operator >> (Message &msg, FragmentIdSet &ids) {
	unsigned int size;
	FragmentId id;
		msg >> size;
	for(unsigned int i = 0;i < size;i++) {
		msg >> id;
		ids.insert(id);
	}
return msg;
}

Message& operator << (Message &msg, const vector<int> &v) {
	unsigned int size = v.size();
		msg << size;
	for(vector<int>::const_iterator it = v.begin();it != v.end();it++) {
		msg << *it;
	}
return msg;
}

Message& operator >> (Message &msg, vector<int> &v) {
	unsigned int size;
		msg >> size;
	v.resize(size);
	for(unsigned int i = 0;i < size;i++) {
		msg >> v[i];
	}
return msg;
}

}
