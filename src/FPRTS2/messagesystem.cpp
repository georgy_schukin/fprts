#include "mpicomm.h"
#include "messagesystem.h"

#include <list>

namespace fprts {

extern MPICommunicator comm;

void MessageSystem::Init(unsigned int sz, unsigned int msg_sz) {
	storage.Allocate(sz, msg_sz);
}

Message* MessageSystem::Get(Message::Tag msg_tag) {
	boost::unique_lock<boost::mutex> lock(ms_mutex);
	Message *msg = storage.Get();
	msg->SetTag(msg_tag);
	access[msg->GetId()] = 0; // init for new message
return msg;
}

void MessageSystem::Free(Message *msg) {
	boost::unique_lock<boost::mutex> lock(ms_mutex);
	unsigned int id = msg->GetId();
	if(access[id] == 0) { // msg is not used anymore
		storage.Free(id); // free it
	} else {
		free_flg.insert(id); // set flg to free later
	}
}

int MessageSystem::Send(Message *msg, NodeId node) { // send msg
	boost::unique_lock<boost::mutex> lock(ms_mutex);
	unsigned int id = msg->GetId();
	tasks.push(pair<unsigned int, NodeId>(id, node)); // push msg in queue to send later
	access[id]++;
return 0;
}

int MessageSystem::SendAndFree(Message *msg, NodeId node) {
	boost::unique_lock<boost::mutex> lock(ms_mutex);
	unsigned int id = msg->GetId();
	tasks.push(pair<unsigned int, NodeId>(id, node)); // push msg in queue to send later
	access[id]++;
	free_flg.insert(id); // set flg to free later
return 0;
}

bool MessageSystem::DoSend() { // do next send task
	boost::unique_lock<boost::mutex> lock(ms_mutex);

	if(tasks.empty()) return false;
	
	unsigned int id = tasks.front().first;
	NodeId node = tasks.front().second;
	tasks.pop();
	
	Message *msg = storage.Get(id);	
	int r_id;
		comm.Isend(msg->Buf(), msg->BufSize(), (int)msg->GetTag(), node, r_id); // send
	isends[r_id] = id;
return true;
}

void MessageSystem::CheckAll() { // check for finished sends
	boost::unique_lock<boost::mutex> lock(ms_mutex);
	list<int> completed;
	for(map<int, unsigned int>::const_iterator it = isends.begin();it != isends.end();it++) { // for all ongoing isends
		unsigned int id = it->second;
		if(comm.CheckCompletion(it->first)) { // send is completed
			access[id]--;
			if((access[id] == 0) && (free_flg.find(id) != free_flg.end())) { // msg isn't used anymore and can be freed
				storage.Free(id);
				free_flg.erase(id);
			}			
			completed.push_back(it->first);
		}		
	}
	for(list<int>::const_iterator it = completed.begin();it != completed.end();it++) isends.erase(*it);	
}
}