#include "mpicomm.h"

namespace fprts
{

int MPICommunicator::Init(Topology::Type tp)
{
    int req = -1;    
	//MPI_Init(0, 0);
    MPI_Init_thread(0, 0, MPI_THREAD_MULTIPLE, &req);
    
    if(req != MPI_THREAD_MULTIPLE)
    {
	    printf("MPICommunicator : ERROR : Can't get required MPI multithread level, get only %d\n", req);
	 //   return -1;
    }	
    
    int rnk, sz;
	MPI_Comm_rank(MPI_COMM_WORLD, &rnk); // get rank
	MPI_Comm_size(MPI_COMM_WORLD, &sz); // get size    

	MPICommunicator::InitMPIComm(tp, rnk, sz, &mpi_comm);

	MPI_Comm_rank(mpi_comm, &rnk); // get rank
	MPI_Comm_size(mpi_comm, &sz); // get size
	rank = rnk;
	size = sz;

	MPICommunicator::InitNeighbours(tp, rank, size, mpi_comm, neigh_nodes);
return 0;
}

void MPICommunicator::InitMPIComm(Topology::Type tp, int rank, int size, MPI_Comm *comm) {	
	switch(tp) {
		case Topology::ALL_TO_ALL: {				
				MPI_Comm_dup(MPI_COMM_WORLD, comm); // use default comm world
				break;
			}
		case Topology::LATTICE: {
				int dim = size;
				int period = 0;
				MPI_Cart_create(MPI_COMM_WORLD, 1, &dim, &period, 0, comm);
				break;
			}
		case Topology::RING: {
				int dim = size;
				int period = 1;
				MPI_Cart_create(MPI_COMM_WORLD, 1, &dim, &period, 0, comm);
				break;
			}
	}	
}

void MPICommunicator::InitNeighbours(Topology::Type tp, int rank, int size, MPI_Comm comm, set<int> &neigh) {	
	switch(tp) {
		case Topology::ALL_TO_ALL: {
				for(int i = 0;i < size;i++) if(rank != i) neigh.insert(i);
				break;
			}
		case Topology::LATTICE:
		case Topology::RING: {
				int src = rank, dest;
				MPI_Cart_shift(comm, 0, 1, &src, &dest);
				if(src != MPI_UNDEFINED) neigh.insert(src);
				if(dest != MPI_UNDEFINED) neigh.insert(dest);
				break;
			}
	}
}


int MPICommunicator::Finalize()
{
	printf("Finalize\n");
	MPI_Comm_free(&mpi_comm);
	MPI_Finalize();
return 0;
}

int MPICommunicator::Send(void *data, int data_sz, int data_tag, int dest_node)
{
	MSL;
	return MPI_Send(data, data_sz, MPI_BYTE, dest_node, data_tag, mpi_comm);
}

int MPICommunicator::Recv(void *data, int data_sz, int data_tag, int src_node)
{
	MSL;
	return MPI_Recv(data, data_sz, MPI_BYTE, src_node, data_tag, mpi_comm, MPI_STATUS_IGNORE);
}

int MPICommunicator::Isend(void *data, int data_sz, int data_tag, int dest_node, int &id)
{	
	MSL;
	id = req_id++;
	return MPI_Isend(data, data_sz, MPI_BYTE, dest_node, data_tag, mpi_comm, &requests[id]);
}

bool MPICommunicator::Check(int node, int data_tag, MPICommunicator::Status& stat)
{
	MSL;
	MPI_Status st;
	int flag = 0;

	MPI_Iprobe(node, data_tag, mpi_comm, &flag, &st); // check for income

	if(flag)
	{
		stat.rank = st.MPI_SOURCE;		
		stat.size = st.count;
		stat.tag =  st.MPI_TAG;
		return true;
	}
return false;
}

bool MPICommunicator::CheckAny(MPICommunicator::Status& stat)
{
	return Check(MPI_ANY_SOURCE, MPI_ANY_TAG, stat);
}

bool MPICommunicator::CheckCompletion(int id)
{
	MSL;
	if(requests.find(id) != requests.end())
	{
		int flg = 0;
		MPI_Test(&requests[id], &flg, MPI_STATUS_IGNORE);
		if(flg)
		{
			requests.erase(id);
			return true;
		}
	}
return false;
}

void MPICommunicator::Barrier()
{
	MPI_Barrier(mpi_comm);
}

void MPICommunicator::Allreduce(void *send, void *recv, int data_sz, MPI_Op op)
{
	MSL;
	MPI_Allreduce(send, recv, data_sz, MPI_INT, op, mpi_comm);
}

void MPICommunicator::Reduce(void *send, void *recv, int data_sz, MPI_Op op, int node)
{
	MSL;
	MPI_Reduce(send, recv, data_sz, MPI_INT, op, node, mpi_comm);
}

/////////////////////////// MPI Async Communicator /////////////////////////////////
/*MPIAsyncCommunicator::MPIAsyncCommunicator() {}

MPIAsyncCommunicator::~MPIAsyncCommunicator()
{
	CheckCurr();

	while(!curr_comm.empty()) // free all data
	{
		delete curr_comm.front();
		curr_comm.pop_front();
	}
}

int MPIAsyncCommunicator::Send(void* data, UInt data_sz, Int data_tag, NodeId dest_node)
{
	AsyncData* as = new AsyncData(data, data_sz);
		curr_comm.push_back(as);
	return MPI_ISend(as->buf.Buf(), as->buf.BufSize(), MPI_BYTE, GetMPIRank(dest_node), GetMPITag(data_tag), mpi_comm, &(as->req));
}

void MPIAsyncCommunicator::CheckCurr() // checl for completed sends
{
	for(list<AsyncData*>::iterator it = curr_comm.begin(); it != curr_comm.end();) // for all current communications
	{
		int flag = 0;
		MPI_Test(&(it->second->req), &flag, MPI_STATUS_IGNORE); // test operation

		if(flag) // is completed
		{			
			delete it2->second;
			list<AsyncData*>::iterator it2 = it++;	
			curr_comm.erase(it2); // delete data
		}
		else it++;
	}
}*/
}
