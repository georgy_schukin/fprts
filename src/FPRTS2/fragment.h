#pragma once

#include "rtstypes.h"
#include "migratable.h"

namespace fprts {

class FragmentProcedure;

class Fragment : public Migratable // fragment in system
{
protected:
	FragmentId id;
	FragmentProcedure *parent; // ptr to parent procedure (comp fragment)

protected:
	virtual void Pack(Message &msg) const; // pack function
	virtual void Unpack(Message &msg); // unpack function

public:
	Fragment() : parent(0) {}
	Fragment(FragmentId i) : id(i), parent(0) {}
	virtual ~Fragment() {}

	void SetParent(FragmentProcedure *p) {parent = p;}

	FragmentId GetId() const {return id;}
	FragmentProcedure* GetParent() const {return parent;}
};

/*class FragmentRange
{
public:
	NodeId n;
	FragmentId first, last;
};*/

}
