#include "buffer.h"

namespace aux
{

Buffer::Buffer() : buf(0), buf_size(0) {}

Buffer::Buffer(unsigned int sz) {
	Resize(sz);
}
	
Buffer::Buffer(void* dt, unsigned int sz) {
	Resize(sz);
	memcpy(buf, dt, sz);
}

Buffer::Buffer(const Buffer& b) {
	Resize(b.BufSize());
	memcpy(buf, b.Buf(), b.BufSize());
}

Buffer::~Buffer() {
	Clear();
}
    
void* Buffer::Resize(unsigned int sz) { // get mem for buffer
	if(sz == 0) Clear();
	else {
		void* res = realloc(buf, sz);
		if(res) {
			buf_size = sz;
			buf = res;
		}			
	}
return buf;
}

void Buffer::Clear() {
	if(buf) free(buf);
	buf_size = 0;
	buf = 0;
}

}



