#include "commsystem.h"
#include "execsystem.h"
#include "codelibrary.h"
#include "fragmentspawner.h"
#include "scheduler.h"
#include "loadlogger.h"

#include <boost/lambda/lambda.hpp>
#include <boost/bind.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/foreach.hpp>
#include <algorithm>

using namespace aux;

namespace fprts {

extern MPICommunicator comm;

double ex_time = 0;

void ExecSystem::ExecCF(CompFragment *cf) {
	CodeFragment *code = code_lib->GetCode(cf->code_id);
	FragmentProcedure *proc = cf->GetParent();
	
	proc->OnCFStart(cf);

	if(code->IsAtom()) { // atomic cf
		ExecAtomCF(cf, (CodeAtom*)code); // exec atom - call function
	} else {
		ExecStructCF(cf, (CodeGraph*)code); // exec struct - create proc, etc.
	}	
	
	OnFinishedCF(proc, cf);
}

void ExecSystem::ExecAtomCF(CompFragment *cf, CodeAtom *code) {
	double t = MPI_Wtime();
	code->Exec(cf->ic); // call atomic func
	ex_time += (MPI_Wtime() - t);
}

void ExecSystem::ExecStructCF(CompFragment *cf, CodeGraph *code) {
	FragmentProcedure *proc = GetNewProc(); // create new proc
	proc->SetParent(cf);
	proc->ic = cf->ic;
	ExecProcedure(proc, code);
}

void ExecSystem::ExecProcedure(FragmentProcedure *proc, CodeGraph *code) {
	FragmentGraph graph;	
		graph.Build(code, proc->ic);		
	Distributor::DistributionPlan plan;
		distributor->Distribute(graph, load_collector, plan); // call distributor to distribute proc
	DistributeGraph(proc->GetId(), graph, plan); // send cfs/dfs to other nodes		
	graph.Intersect(ThisNode(), plan); // remove moved vertices from graph
	fls[proc->GetId()].OnLocationChange(plan); // init loc storage on proc main node	
	proc->OnNewGraph(graph);
	OnNewProc(proc); // add proc to storage
}

void ExecSystem::DistributeGraph(ProcedureId proc_id, FragmentGraph &graph, Distributor::DistributionPlan &plan) { 
	NodeId this_node = ThisNode();
	BOOST_FOREACH(const Distributor::DistributionPlan::value_type &d, plan) { // send to other nodes
		if(d.first != this_node) {
			comm_system->SendGraph(proc_id, graph, plan, d.first);
		}
	}
}

void ExecSystem::CheckReadyCFs(FragmentProcedure *proc) {	
	list<FragmentId> cfs;
	Scheduler::SchedulePlan plan;
	sched_mutex.lock();
		scheduler->Schedule(proc->GetGraph(), proc->GetReadyCFs(), plan);
	tp_mutex.lock();
	BOOST_FOREACH(const Scheduler::SchedulePlan::value_type &s, plan) {	
		list<CompFragment*> res;
		proc->OnSchedule(s.second, res);
		task_pool->Add(s.first, res);
	}	
	tp_mutex.unlock();
	sched_mutex.unlock();
}

void ExecSystem::CheckRequiredDFs(FragmentProcedure *proc) {
	FragmentIdSet req_dfs;
	proc->PopDFRequests(req_dfs);
	if(req_dfs.empty()) return;
	if(proc->GetNodeId() != ThisNode()) { // loc center is on other node
		comm_system->SendDFRequest(proc->GetId(), req_dfs, ThisNode(), proc->GetNodeId()); // send request to loc cneter
	} else { // this node is loc center
		FragmentLocationStorage &loc = fls[proc->GetId()]; // get loc storage
		FragmentLocationStorage::LocationNodeMap loc_map;
		loc.GetDFsLocation(req_dfs, loc_map); // get locations
		BOOST_FOREACH(const FragmentLocationStorage::LocationNodeMap::value_type &l, loc_map) {
			comm_system->SendDFRequest(proc->GetId(), l.second, ThisNode(), l.first); // redirect requests to other nodes
		}
	}
}

void ExecSystem::SatisfyDeps(FragmentProcedure *proc, const FragmentProcedure::CFDepMap &deps) { // satisfy deps for local cfs
	FragmentProcedure::CFDepMap remote_deps;
	proc->OnCFDepend(deps, remote_deps); // satisfy local deps
	RedirectDeps(proc->GetId(), remote_deps);
}
	

void ExecSystem::RedirectDeps(ProcedureId proc_id, const FragmentProcedure::CFDepMap &deps) { // send deps to remote cfs
	if(deps.empty()) return; // nothing to do
	if(proc_id.first != ThisNode()) { // location storage is on other node
		comm_system->SendDepend(proc_id, deps, proc_id.first); // redirect through main node
	} else { // location storage is on this node
		FragmentIdSet cf_ids;
		FragmentLocationStorage::LocationNodeMap loc;
		boost::copy(deps | boost::adaptors::map_keys, std::inserter(cf_ids, cf_ids.end())); // get cf ids
		fls[proc_id].GetCFsLocation(cf_ids, loc); // get locations of cfs
		BOOST_FOREACH(const FragmentLocationStorage::LocationNodeMap::value_type &l, loc) { // for each remote ode
			FragmentProcedure::CFDepMap dp;
			BOOST_FOREACH(FragmentId cf_id, l.second) { // init deps for remote node
				dp[cf_id] = deps.find(cf_id)->second;
			}
			comm_system->SendDepend(proc_id, dp, l.first); // send to node
		}
	}
}

void ExecSystem::RemoveProc(ProcedureId proc_id) {
	boost::unique_lock<boost::mutex> lock(proc_mutex);	
	ps.RemoveProc(proc_id); // erase procedure
	fls.erase(proc_id); // erase fls
}

void ExecSystem::OnNewProc(FragmentProcedure *proc) {
	boost::unique_lock<boost::mutex> lock(proc_mutex);	
	ps.AddProc(proc);
	CheckReadyCFs(proc);
	CheckRequiredDFs(proc);
}

void ExecSystem::OnFinishedProc(FragmentProcedure *proc, NodeId src) {
	ProcedureId proc_id = proc->GetId();
	if(proc_id.first != ThisNode()) { // proc main node is another node
		comm_system->SendDone(proc_id, ThisNode(), proc_id.first); // signal to main node
		if(proc->IsEmpty()) { // proc is empty
			RemoveProc(proc_id); // delete then (if not may forget to delete later)
		}
	} else { // this is proc main node (src - id of node when proc is finished)
		FragmentLocationStorage &fl = fls[proc_id];
		fl.RemoveCFsLocation(src); // remove cfs info for src node
		if(fl.CFsEmpty()) { // total empty - proc is truly finished
			CompFragment *cf = proc->GetParent();
			if(cf && cf->GetParent()) 
				OnFinishedCF(cf->GetParent(), cf); // signal to parent about finish
			set<NodeId> nodes;	
				fl.GetDFsLocation(nodes); // get all nodes on whis proc parts is located (by dfs)
				nodes.erase(ThisNode());
			BOOST_FOREACH(NodeId node, nodes) {
				comm_system->SendDone(proc_id, ThisNode(), node); // inform other nodes to remove proc
			}
			RemoveProc(proc_id); // remove on main
		}
	}
}

void ExecSystem::OnFinishedCF(FragmentProcedure *proc, CompFragment *cf) { // when cf is finished
	FragmentProcedure::CFDepMap cf_deps;	
	proc->OnCFDone(cf, cf_deps); // signal to proc about done cf

	if(proc->IsDone()) { // proc is finished - no cfs in it
		printf("DONE\n");
		RedirectDeps(proc->GetId(), cf_deps); // send deps to other nodes, if any
		OnFinishedProc(proc, ThisNode()); // finish proc
	} else { // there are cfs in proc
		SatisfyDeps(proc, cf_deps); // satisfy immediately
		CheckReadyCFs(proc); // check ready for proc
		CheckRequiredDFs(proc);
	}
}

void ExecSystem::OnNewDFs(ProcedureId proc_id, const FragmentProcedure::DFList &dfs) {
	FragmentProcedure *proc = GetProc(proc_id);
	proc->OnNewDFs(dfs);	// add to proc
	CheckReadyCFs(proc);
}

void ExecSystem::OnNewCFs(ProcedureId proc_id, const FragmentProcedure::CFList &cfs) {
	FragmentProcedure *proc = GetProc(proc_id);
	proc->OnNewCFs(cfs);	// add to proc
	CheckReadyCFs(proc);
}

void ExecSystem::OnNewGraph(ProcedureId proc_id, const FragmentGraph &graph) { 
	FragmentProcedure *proc = GetProc(proc_id);
	proc->OnNewGraph(graph);
	CheckReadyCFs(proc);
}

void ExecSystem::OnDFRequest(ProcedureId proc_id, const FragmentIdSet &df_ids, NodeId src) { // recv request for dfs
	FragmentProcedure *proc = GetProc(proc_id);
	FragmentIdSet rest;
	FragmentProcedure::DFList dfs; 
	bool res = proc->RequestDFs(df_ids, dfs, rest); // get dfs
	comm_system->SendDFs(proc_id, dfs, src); // send dfs back to source
	if(!res) { // some dfs are left
	}
	FragmentLocationStorage::LocationNodeMap loc;
	loc[src] = df_ids;
	if(proc_id.first == ThisNode()) { // main node
		fls[proc_id].SetDFsLocation(loc);
	} else {
		comm_system->SendLocation(proc_id, loc, true, proc_id.first); // inform main node
	}
}

void ExecSystem::OnCFDepend(ProcedureId proc_id, const FragmentProcedure::CFDepMap &cf_deps) {
	FragmentProcedure *proc = GetExistingProc(proc_id);
	if(!proc) { // no proc on node
		RedirectDeps(proc_id, cf_deps); // redirect only
	} else {
		SatisfyDeps(proc, cf_deps); // satisfy local (and maybe redirect later)
	}
}

void ExecSystem::OnProcDone(ProcedureId proc_id, NodeId src) {
	if(proc_id.first == ThisNode()) { // this is proc main node - message is from another node
		FragmentProcedure *proc = GetExistingProc(proc_id);
		if(proc) OnFinishedProc(proc, src);
	} else { // message is from main node
		RemoveProc(proc_id); // remove local proc storage
	}
}

void ExecSystem::OnDFLocation(ProcedureId proc_id, const FragmentLocationStorage::LocationNodeMap &loc) {
	fls[proc_id].SetDFsLocation(loc);
}

void ExecSystem::OnCFLocation(ProcedureId proc_id, const FragmentLocationStorage::LocationNodeMap &loc) {
	fls[proc_id].SetCFsLocation(loc);
}

void ExecSystem::Init() {
	load_collector.Init(comm_system->GetTopology()); // prepare load info
	ps.Init(ThisNode());

	scheduler = new SimpleScheduler(); // create help objects
	task_pool = new SimpleTaskPool();
	distributor = new RandomDistributor(); 
}

void ExecSystem::Finalyze() {
	delete scheduler;
	delete task_pool;
	delete distributor;
}

int ExecSystem::Start(int t_num) {
	is_working = true;
		for(int i = 0;i < t_num;i++) exec_threads.create_thread(boost::bind(&ExecSystem::ExecFunc, this, i));	
		load_thread = boost::thread(boost::bind(&ExecSystem::LoadFunc, this));	
return 0;
}

int ExecSystem::Stop() {	
	load_thread.join();
		is_working = false;
	exec_threads.join_all();
return 0;
}

void ExecSystem::ExecFunc(int t_id) {
	//int ex_cnt = 0;
	list<CompFragment*> cfs;

	while(is_working) {
		tp_mutex.lock();
		cfs.clear();
		if(task_pool->Get(t_id, cfs, 10)) {
			tp_mutex.unlock();
			for(list<CompFragment*>::iterator it = cfs.begin();it != cfs.end();it++) {
				ExecCF(*it);
			}
		} else {
			tp_mutex.unlock();
			boost::this_thread::sleep(boost::posix_time::millisec(50));
		}
	}
	printf("Exec time : %.5f\n" ,ex_time);
}

void ExecSystem::LoadFunc() {	
	NodeId rank = ThisNode();
	double start_time = MPI_Wtime();
	set<NodeId> nodes;	

	LoadCollector::LoadInfo info;
	vector<LoadCollector::LoadInfo> loadv(comm.GetSize());

	LoadLogger *logger = 0;

	if(!rank) {
		logger = new LoadLogger("log.txt");
	}

	load_collector.GetArea(nodes); // get neighbour nodes

	while(is_working) {
		int fn = IsFinished() ? 0 : 1;
		int working = fn;
			comm.Allreduce(&fn, &working, 1, MPI_MAX);
			//MPI_Allreduce(&working, &fn, 1, MPI_INT, MPI_MAX, comm.mpi_comm); // gather flg from all nodes
			printf("Rank %d check: %d (%d)\n", rank, working, fn);
		if(working == 0) { // all is finished
			is_working = false;
		} else {
			boost::this_thread::sleep(boost::posix_time::millisec(500)); // wait
		}

		/*lc_mutex.lock();
			info = load_collector.GetCurrInfo(); // copy current load info
		lc_mutex.unlock();
		
		MPI_Allgather(&info, sizeof(LoadCollector::LoadInfo), MPI_BYTE, 
			&(loadv[0]), sizeof(LoadCollector::LoadInfo), MPI_BYTE, comm.mpi_comm); // gather load info from all nodes (simple algorithm)

		lc_mutex.lock();
			for(set<NodeId>::const_iterator it = nodes.begin();it != nodes.end();it++) { // update load info
				load_collector.UpdateInfo(*it, loadv[*it]);
			}
		lc_mutex.unlock();*/		

		if(!rank) {
			double time = MPI_Wtime(); // get stamp time
			for(int i = 0;i < comm.GetSize();i++) { // log for everybody
				logger->Log(i, time - start_time, loadv[i]);
			}
		}
	}	
}

NodeId ExecSystem::ThisNode() const {
	return comm.GetRank();
}

bool ExecSystem::IsFinished() {
	boost::unique_lock<boost::mutex> lock(proc_mutex);
return ps.IsEmpty(); // finish when all procs are done
}

FragmentProcedure* ExecSystem::GetNewProc() {
	boost::unique_lock<boost::mutex> lock(proc_mutex);
return ps.NewProc();
}

FragmentProcedure* ExecSystem::GetProc(ProcedureId proc_id) {
	boost::unique_lock<boost::mutex> lock(proc_mutex);
return ps.GetProc(proc_id);
}

FragmentProcedure* ExecSystem::GetExistingProc(ProcedureId proc_id) {
	boost::unique_lock<boost::mutex> lock(proc_mutex);
return ps.ProcExists(proc_id) ? ps.GetProc(proc_id) : 0;
}

}
