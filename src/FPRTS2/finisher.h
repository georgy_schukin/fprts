#pragma once

namespace fprts
{

///////////////////////// Finisher //////////////////////////////
class Finisher // to check completion of work
{
protected:
	int alldone;
	int alldone_count;

	MyMutex f_mutex;
	MyCond f_cond;

public:
	Finisher() : alldone(0), alldone_count(0) {}
	~Finisher() {}

	void Init();

	void SetData(int dn, int size);
	void SetDataAndWait(int dn, int size);

	void Wait();
	void WakeUp();

	void GetData(int &all, int &cnt);	
};

}