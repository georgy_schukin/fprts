#include "messagestorage.h"

namespace fprts {

int MessageStorage::Allocate(unsigned int num, unsigned int msg_sz) {
	unsigned int sz = messages.size();
	for(unsigned int i = 0;i < num;i++)
	{
		messages.push_back(MessagePair(boost::shared_ptr<Message>(new Message(sz + i, msg_sz)), true)); // msg id = msg index in vector
	}
return 0;
}

void MessageStorage::Clear()
{
	messages.clear();
}

Message* MessageStorage::Get()
{
	for(std::vector<MessagePair>::iterator it = messages.begin();it != messages.end();it++) // search through messages
	{
		if((*it).second) // find free message
		{
			(*it).second = false;

			Message *msg = (*it).first.get();			
				msg->SetReadPos(0);
				msg->SetWritePos(0);
			return msg; // return message
		}
	}	
	unsigned int sz = messages.size();
	boost::shared_ptr<Message> m(new Message(sz, 1024)); // create new message
	messages.push_back(MessagePair(m, false)); // add as not free
return m.get();
}

Message* MessageStorage::Get(unsigned int id)
{
	if(id < messages.size())
	{
		return messages[id].first.get();
	}
return 0;
}

int MessageStorage::Free(Message* msg)
{
	const unsigned int id = msg->GetId();
	if(id < messages.size())
	{				
		messages[id].second = true; 
		return 0;		
	}
return -1;
}

int MessageStorage::Free(unsigned int id)
{
	if(id < messages.size())
	{				
		messages[id].second = true; 
		return 0;		
	}
return -1;
}

}