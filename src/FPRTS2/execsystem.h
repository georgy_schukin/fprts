#pragma once

#include "datafragment.h"
#include "compfragment.h"
#include "codelibrary.h"
#include "loadcollector.h"
#include "distributor.h"
#include "fragmentprocedure.h"
#include "fragmentprocstorage.h"
#include "fragmentlocationstorage.h"
#include "taskpool.h"

#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

using namespace std;

namespace fprts {

class CommSystem;
class CodeLibrary;
class Scheduler;
class FragmentSpawner;

class ExecSystem // exec subsystem on a node to run fragments
{
protected:
	class ThreadContext
	{
	public:
		double ex_time;
	public:
		ThreadContext() : ex_time(0) {}
		~ThreadContext() {}
	};

protected:
	CommSystem *comm_system;	
	CodeLibrary *code_lib;	

	FragmentProcStorage ps;
	LoadCollector load_collector;

	map<ProcedureId, FragmentLocationStorage> fls; // locations

	TaskPool *task_pool;
	Scheduler *scheduler;
	Distributor *distributor;

	volatile bool is_working;

	boost::mutex proc_mutex, sched_mutex, tp_mutex, lc_mutex;

	boost::thread_group exec_threads;
	boost::thread load_thread;

protected:
	void ExecFunc(int t_id); // for exec threads		
	void LoadFunc(); // check for completion, gather load info

protected:
	void ExecCF(CompFragment *cf); // func to exec ready cf
	void ExecAtomCF(CompFragment *cf, CodeAtom *code);
	void ExecStructCF(CompFragment *cf, CodeGraph *code);
	void ExecProcedure(FragmentProcedure *proc, CodeGraph *code);

	void CheckInput(CompFragment *cf, InputContext &res);
	void CheckReadyCFs(FragmentProcedure *proc);
	void CheckRequiredDFs(FragmentProcedure *proc);

	void SatisfyDeps(FragmentProcedure *proc, const FragmentProcedure::CFDepMap &deps);
	void RedirectDeps(ProcedureId proc_id, const FragmentProcedure::CFDepMap &deps);

	void RemoveProc(ProcedureId proc_id);

protected:	
	void OnNewProc(FragmentProcedure *proc);
	void OnFinishedProc(FragmentProcedure *proc, NodeId src);
	void OnFinishedCF(FragmentProcedure *proc, CompFragment *cf);

public:
	void OnNewDFs(ProcedureId proc_id, const FragmentProcedure::DFList &dfs);
	void OnNewCFs(ProcedureId proc_id, const FragmentProcedure::CFList &cfs);
	void OnNewGraph(ProcedureId proc_id, const FragmentGraph &graph);
	void OnDFRequest(ProcedureId proc_id, const FragmentIdSet &df_ids, NodeId src);
	void OnCFDepend(ProcedureId proc_id, const FragmentProcedure::CFDepMap &cf_deps);
	void OnProcDone(ProcedureId proc_id, NodeId src);
	void OnDFLocation(ProcedureId proc_id, const FragmentLocationStorage::LocationNodeMap &loc);
	void OnCFLocation(ProcedureId proc_id, const FragmentLocationStorage::LocationNodeMap &loc);

	bool IsFinished();

protected:
	void DistributeGraph(ProcedureId proc_id, FragmentGraph &graph, Distributor::DistributionPlan &plan); 

protected:
	friend class FPRTS;
	void SetCommSystem(CommSystem *cm) {comm_system = cm;}
	void SetCodeLibrary(CodeLibrary *code) {code_lib = code;}

public:
	ExecSystem() : is_working(false) {}
	~ExecSystem() {}

	FragmentProcedure* GetNewProc();
	FragmentProcedure* GetProc(ProcedureId proc_id);
	FragmentProcedure* GetExistingProc(ProcedureId proc_id);
	//bool ProcExists(ProcedureId proc_id);

	void Init();
	void Finalyze();

	int Start(int t_num = 1); // start threads
	int Stop(); // wait for completion

	NodeId ThisNode() const;
};

}
