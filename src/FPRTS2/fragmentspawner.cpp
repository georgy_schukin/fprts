#include "fragmentspawner.h"
#include "codegraph.h"
#include "fragmentprocedure.h"

namespace fprts {

bool BasicFragmentSpawner::Spawn() { // init proc by graph	
	const CodeGraph::DataVertexMap &dv = graph->data_vertices;
	const CodeGraph::CallVertexMap &cv = graph->call_vertices;
	const CodeGraph::ArcMap &arcs_in = graph->arcs_in;
	const CodeGraph::ArcMap &arcs_out = graph->arcs_out;

	for(CodeGraph::DataVertexMap::const_iterator it = dv.begin();it != dv.end();it++) {
		ProcessData(it->second);
	}

	for(CodeGraph::CallVertexMap::const_iterator it = cv.begin();it != cv.end();it++) {
		ProcessCall(it->second);
	}
		
	for(CodeGraph::ArcMap::const_iterator it = arcs_in.begin();it != arcs_in.end();it++) { // for each vertex with incoming arcs
		proc->GetCF(v_to_f[it->first])->dep_cnt = it->second.size(); // set dep cnt for num of incoming arcs
	}
	for(CodeGraph::ArcMap::const_iterator it = arcs_out.begin();it != arcs_out.end();it++) // for each vertex with outcoming arcs
	{
		FragmentIdSet out;
		for(set<VertexId>::const_iterator it2 = it->second.begin();it2 != it->second.end();it2++) {
			out.insert(v_to_f[*it2]);
		}
			proc->GetCF(v_to_f[it->first])->arcs_out = out; // create arcs for proc
	}
return true;
}

FragmentId BasicFragmentSpawner::ProcessData(const VertexData &v) {
	FragmentId id = proc->GetNextId();
	DataFragment *df = DataFragment::New(v.data_type, id, v.data_params);
	df->SetParent(proc);
	proc->AddDF(df);
	v_to_f[v.GetId()] = id;
return id;
}

FragmentId BasicFragmentSpawner::ProcessCall(const VertexCall &v) {
	FragmentId id = proc->GetNextId();
	CompFragment *cf = new CompFragment(id, v.code_id);
	cf->SetParent(proc);
	cf->iac.input_sz = v.input_sz;
	for(unsigned int i = 0;i < v.input_sz + v.output_sz;i++) { // fill args
		cf->iac.args.push_back(ResolveArg(v.args[i]));
	}
	proc->AddCF(cf);
	v_to_f[v.GetId()] = id;
return id;
}

InputArgContext::Arg BasicFragmentSpawner::ResolveArg(const VertexCall::Arg &arg) {
	InputArgContext::Arg res;
	switch(arg.arg_type) {
		case VertexCall::Arg::LOCAL: { // arg is local df
			res.id = v_to_f[arg.id]; // arg is is data vertex id; convert to df id
			res.local = true; 
			break;
		}
		case VertexCall::Arg::INPUT: { // arg is input
			res.id = proc->ic.args[arg.id]->GetId(); // get id from input context
			res.local = false;
			break;
		}
	}
return res;
}

}
