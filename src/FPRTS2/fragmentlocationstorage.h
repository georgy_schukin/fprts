#pragma once

#include "rtstypes.h"
#include "distributor.h"
#include <boost/thread/mutex.hpp>
#include <map>

using namespace std;

namespace fprts {

class FragmentLocationStorage // location info for fragment procedure
{
public:
	typedef map<NodeId, FragmentIdSet> LocationNodeMap;
	typedef map<FragmentId, NodeId> LocationMap;

protected:
	LocationMap dfs_location;
	LocationMap cfs_location;

	boost::mutex loc_mutex;

public:
	FragmentLocationStorage() {}
	FragmentLocationStorage(const FragmentLocationStorage &l) {}
	~FragmentLocationStorage() {}

	void OnLocationChange(const Distributor::DistributionPlan &plan);

	void SetDFsLocation(const LocationNodeMap &loc);
	void SetCFsLocation(const LocationNodeMap &loc);

	void GetDFsLocation(const FragmentIdSet &dfs, LocationNodeMap &loc);
	void GetCFsLocation(const FragmentIdSet &cfs, LocationNodeMap &loc);

	void GetDFsLocation(set<NodeId> &nodes);
	void GetCFsLocation(set<NodeId> &nodes);

	void RemoveDFsLocation(const FragmentIdSet &dfs);
	void RemoveCFsLocation(const FragmentIdSet &cfs);

	void RemoveDFsLocation(NodeId node);
	void RemoveCFsLocation(NodeId node);

	bool CFsEmpty();
	bool DFsEmpty();
};

}
