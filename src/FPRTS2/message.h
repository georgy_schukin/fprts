#pragma once

#include "rtstypes.h"
#include "rwbuffer.h"

using namespace aux;

namespace fprts {

class Message : public ReadWriteBuffer // message - buffer of data
{
public:
	enum Tag
	{
		NONE = 0,
		DF,			// send dfs
		CF,			// send cfs
		GRAPH,	
		CF_DEPEND,
		PROC_DONE,
		DF_LOCATION,
		CF_LOCATION,
		DF_REQUEST
	};

protected:	
	unsigned int id; // some id of message
    Tag tag; // tag of message

public:    
    Message() : id(0), tag(Message::NONE) {}
	//Message(unsigned int sz) : id(0), tag(Message::NONE) {Resize(sz);}    	
	Message(unsigned int i, unsigned int sz) : id(i), tag(Message::NONE) {Resize(sz);}    	
	//Message(unsigned int i, Message::Tag t, unsigned int sz) : id(i), tag(t) {Resize(sz);}    	
    ~Message() {}    
	
	void SetId(unsigned int i) {id = i;}
	void SetTag(Message::Tag t) {tag = t;}
 	
	unsigned int GetId() const {return id;}	
	Tag GetTag() const {return tag;}
};

Message& operator << (Message &msg, const ProcedureId &id);
Message& operator >> (Message &msg, ProcedureId &id);

Message& operator << (Message &msg, const FragmentIdSet &ids);
Message& operator >> (Message &msg, FragmentIdSet &ids);

Message& operator << (Message &msg, const vector<int> &v);
Message& operator >> (Message &msg, vector<int> &v);

}
