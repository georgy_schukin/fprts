#pragma once

#include "rtstypes.h"
#include <string>

using namespace std;

namespace fprts
{

namespace idgen
{
	FragmentId Id(const string &str); // get id from string

	string Concat(const string &s1, const string &s2); // concat
	string Subname(const string &s1, const string &s2); // get s1.s2

	string Index(const string &str, int i); // get name[i]
	string Index(const string &str, int i, int j); // get name[i,j]
	string Index(const string &str, int i, int j, int k); // get name[i,j,k]
}

}
