#pragma once

#include "fragmentstorage.h"
#include "fragmentgraph.h"
#include "distributor.h"
#include "context.h"
#include "codegraph.h"
#include <boost/thread/mutex.hpp>
#include <list>
#include <map>

using namespace std;

namespace fprts {

class FragmentProcedure
{	
public:
	typedef list<pair<FragmentGraph::DFVertex, DataFragment*> > DFList;
	typedef list<FragmentGraph::CFVertex> CFList;
	typedef map<FragmentId, unsigned int> CFDepMap;

protected:
	ProcedureId proc_id; // id of proc (to find where proc was spawned)	
	CompFragment *parent; // cf which spawned this proc (0 if main proc)

	FragmentStorage fs;
	FragmentGraph fg;
	
	set<FragmentId> ready_cfs; // cfs whic are ready
	//set<FragmentId> free_cfs;

	map<FragmentId, FragmentIdSet> required_dfs; // dfs needed for cfs
	FragmentIdSet dfs_requests; // requests for dfs
	//map<FragmentId, unsigned int> cfs_deps;

	boost::mutex p_mutex; // mutex for opeations with proc

public:
	InputContext ic; // input args	

protected:	
	bool IsFree(const FragmentGraph::CFVertex &cv);
	bool IsReady(const FragmentGraph::CFVertex &cv, FragmentIdSet &req_dfs);

	bool CheckCF(const FragmentGraph::CFVertex &cv);	

	void InitInputContext(InputContext &cf_ic, const InputArgContext &cf_iac);
	
public:	
	FragmentProcedure(const ProcedureId &id) : proc_id(id), parent(0) {}
	~FragmentProcedure() {}

	void SetParent(CompFragment *cf) {parent = cf;}

	ProcedureId GetId() const {return proc_id;}
	NodeId GetNodeId() const {return proc_id.first;}
	FragmentId GetLocalId() const {return proc_id.second;}
	CompFragment* GetParent() const {return parent;}
	
	void OnInit(); // init proc on node (find free and ready cfs, etc.)

	void OnNewDF(const FragmentGraph::DFVertex &dv, DataFragment *df);	 // when add df from another node
	void OnNewDFs(const DFList &dfs); // when add dfs from another nodes
	void OnNewCF(const FragmentGraph::CFVertex &cv); // when add cf from another node
	void OnNewCFs(const CFList &cfs); // when add cfs from another nodes
	void OnNewGraph(const FragmentGraph &graph); // when new vertices (dfs and cfs) arrived

	void OnCFStart(CompFragment *cf); // mark cf as executing
	void OnCFDone(CompFragment *cf, CFDepMap &cf_deps); // mark cf as finished
	void OnCFDepend(const CFDepMap &cf_deps, CFDepMap &remote_cf_deps); // decrease deps for cfs

	void OnDFsMigrate(const FragmentIdSet &df_ids); 
	void OnCFsMigrate(const FragmentIdSet &cf_ids);
	
	void OnSchedule(const list<FragmentId> &cf_ids, list<CompFragment*> &res); // create cfs for scheduled tasks

	void PopDFRequests(FragmentIdSet &req_dfs);

	bool RequestDFs(const FragmentIdSet &df_ids, DFList &dfs, FragmentIdSet &rest);

	bool IsDone(); // check if proc is done	
	bool IsEmpty(); // check if there are no cfs in proc

	FragmentGraph& GetGraph() {return fg;}
	FragmentStorage& GetStorage() {return fs;}
	const FragmentIdSet& GetReadyCFs() const {return ready_cfs;}
};

}
