#include "fragmentlocationstorage.h"
#include <boost/foreach.hpp>

namespace fprts {

void FragmentLocationStorage::OnLocationChange(const Distributor::DistributionPlan &plan) { // to be called on proc head node
	boost::unique_lock<boost::mutex> lock(loc_mutex);	
	BOOST_FOREACH(const Distributor::DistributionPlan::value_type &p, plan) {
		BOOST_FOREACH(FragmentId df_id, p.second.dfs) {
			dfs_location[df_id] = p.first;
		}
		BOOST_FOREACH(FragmentId cf_id, p.second.cfs) {
			cfs_location[cf_id] = p.first;
		}
	}
}

void FragmentLocationStorage::SetDFsLocation(const LocationNodeMap &loc) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(const LocationNodeMap::value_type &l, loc)
	BOOST_FOREACH(FragmentId df_id, l.second)
	{
		dfs_location[df_id] = l.first;
	}
}

void FragmentLocationStorage::SetCFsLocation(const LocationNodeMap &loc) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(const LocationNodeMap::value_type &l, loc)
	BOOST_FOREACH(FragmentId cf_id, l.second)
	{
		cfs_location[cf_id] = l.first;
	}
}


void FragmentLocationStorage::GetDFsLocation(const FragmentIdSet &dfs, LocationNodeMap &loc) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(FragmentId df_id, dfs) {		
		loc[dfs_location[df_id]].insert(df_id);
	}
}

void FragmentLocationStorage::GetCFsLocation(const FragmentIdSet &cfs, LocationNodeMap &loc) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(FragmentId cf_id, cfs) {		
		loc[cfs_location[cf_id]].insert(cf_id);
	}
}

void FragmentLocationStorage::GetDFsLocation(set<NodeId> &nodes) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(const LocationMap::value_type &l, dfs_location) {	
		nodes.insert(l.second);
	}
}

void FragmentLocationStorage::GetCFsLocation(set<NodeId> &nodes) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(const LocationMap::value_type &l, cfs_location) {	
		nodes.insert(l.second);
	}
}

void FragmentLocationStorage::RemoveDFsLocation(const FragmentIdSet &dfs) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(FragmentId df_id, dfs) {		
		dfs_location.erase(df_id);
	}
}

void FragmentLocationStorage::RemoveCFsLocation(const FragmentIdSet &cfs) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);		
	BOOST_FOREACH(FragmentId cf_id, cfs) {		
		cfs_location.erase(cf_id);
	}
}

void FragmentLocationStorage::RemoveDFsLocation(NodeId node) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);
	list<FragmentId> remove;
	BOOST_FOREACH(const LocationMap::value_type &l, cfs_location) {	
		if(l.second == node) remove.push_back(l.first);
	}
	BOOST_FOREACH(FragmentId df_id, remove) {
		dfs_location.erase(df_id);
	}
}

void FragmentLocationStorage::RemoveCFsLocation(NodeId node) {
	boost::unique_lock<boost::mutex> lock(loc_mutex);
	list<FragmentId> remove;
	BOOST_FOREACH(const LocationMap::value_type &l, cfs_location) {	
		if(l.second == node) remove.push_back(l.first);
	}
	BOOST_FOREACH(FragmentId cf_id, remove) {
		cfs_location.erase(cf_id);
	}
}

bool FragmentLocationStorage::DFsEmpty() {
	boost::unique_lock<boost::mutex> lock(loc_mutex);	
return dfs_location.empty();
}

bool FragmentLocationStorage::CFsEmpty() {
	boost::unique_lock<boost::mutex> lock(loc_mutex);	
return cfs_location.empty();
}

}