#pragma once

#include "rtstypes.h"

namespace rts
{

class StatCollector;

///////////////////////// Load Balancer ///////////////////////
class LoadBalancer
{
public:
	virtual bool Analyze(StatCollector* st = 0) = 0;
	virtual int GetCFToSend(Map<NodeId, set<SystemId> >& cfs_to_send) = 0;
};

class SimpleLoadBalancer : public LoadBalancer
{
public:
	virtual bool Analyze(StatCollector* st = 0) = 0;
	virtual int GetCFToSend(Map<NodeId, set<SystemId> >& cfs_to_send) = 0;
};

}