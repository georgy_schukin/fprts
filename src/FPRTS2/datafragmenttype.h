#pragma once

#include "rtstypes.h"

namespace fprts
{

/////////////////////// Data Fragment Atom Type ///////////////////////////////
class AtomType // type of atom object
{
};

class ATBlock : public AtomType
{
public:
	int size;
};

/////////////////////// Data Fragment Type ////////////////////////////////////
class DataFragmentType // type of data fragment - struct, which describes df type
{
public:
	DataFragmentType() {}
	virtual ~DataFragmentType() {}
	virtual DFType Type() const {return DFT_UNKNOWN;}

	virtual bool IsAtom() const {return true;}
	virtual bool IsComplex() const {return false;}
};

class DFTScalar : public DataFragmentType // scalar atom df
{
protected:
	AtomType* atom;

public:
	DFTScalar() {}
	virtual ~DFTScalar() {}
	virtual DFType Type() const {return DFT_SCALAR;}
};

class DFTArray : public DataFragmentType // array df
{
public:
	DataFragmentType* elem; // type of element
	vector<int> sizes; // sizes of array (-1 = undefined)

public:
	DFTArray() {}
	virtual ~DFTArray() {}
	virtual DFType Type() const {return DFT_ARRAY;}
};

class DFTStruct : public DataFragmentType // struct df
{
public:
	vector<DataFragmentType*> elems;

public:
	DFTStruct() {}
	virtual ~DFTStruct() {}
	virtual DFType Type() const {return DFT_STRUCT;}
};

}
