#pragma once

#include <stdlib.h>
#include <string.h>

namespace aux
{

class Buffer // buffer for data storage
{
protected:
    void* buf; // buffer for header + data    
    unsigned int buf_size; // total size of buf (in bytes)       

protected:
    
public:
	Buffer();
    Buffer(unsigned int sz);
	Buffer(void* dt, unsigned int sz);
	Buffer(const Buffer& buf);
    ~Buffer();
    	
	void* Resize(unsigned int sz); // resize for new sz
	void Clear();

	void* Buf() const {return buf;} // get data
    unsigned int BufSize() const {return buf_size;} // get size of data buf
    
    bool IsEmpty() const {return (buf_size == 0);} // check if buf is empty
};

}
