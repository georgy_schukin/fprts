#pragma once

#include "rtstypes.h"

namespace fprts
{

class Archiver
{
public:
	virtual ArchiverMode Mode() const = 0;	
};

}
