#pragma once

#include "rtstypes.h"
#include "argument.h"
#include "datafragment.h"

#include <vector>
#include <map>
#include <set>

using namespace std;

namespace fprts {

class CodeVertex // vertex of the code graph
{
protected:
	VertexId id; // id of vertex in graph

public:
	CodeVertex() {}
	CodeVertex(VertexId i) : id(i) {}
	virtual ~CodeVertex() {}

	void SetId(VertexId i) {id = i;}

	VertexId GetId() const {return id;}

	//virtual VertexType Type() const = 0;

	//static CodeVertex* New(VertexType type, VertexId id);
};

class VertexData : public CodeVertex // creation of data fragment
{
public:
	//typedef pair<DFType, vector<int> > DataInfo;

public:
	//string data_name;
	DataFragment::Type data_type;
	vector<int> data_params;
	//DataInfo data_info;

public:
	VertexData() {}
	VertexData(VertexId i) : CodeVertex(i) {}
	~VertexData() {}

	//VertexType Type() const {return VT_DATA;}

	void SetData(DataFragment::Type type, const vector<int>& params);
};

class VertexCall : public CodeVertex // call of another code fragment
{
public:
	class Arg {
	public:
		enum ArgType {						
			LOCAL = 0,
			LOCAL_IND,
			INPUT,
			INPUT_IND
		} arg_type;
		unsigned int id;
		unsigned int index;

	public:		
		Arg(Arg::ArgType tp, unsigned int i) : arg_type(tp), id(i), index(0) {}
		Arg(Arg::ArgType tp, unsigned int i, unsigned int idx) : arg_type(tp), id(i), index(idx) {}
		~Arg() {}		
	};

public:
	FragmentId code_id;
	//vector<Arg> input, output, params; // fact args (expressions)
	vector<Arg> args;
	unsigned int input_sz, output_sz;

public:
	VertexCall() {}
	VertexCall(VertexId i) : CodeVertex(i) {}
	~VertexCall() {}

	//VertexType Type() const {return VT_CALL;}

	void SetCodeId(FragmentId code) {code_id = code;}
	void SetArgs(const vector<VertexCall::Arg> &args, unsigned int i_sz, unsigned int o_sz);

	/*void AddInput(const string &in);
	void AddOutput(const string &out);
	void AddParam(const string &param);*/
};

/*class VertexIndexed : public ControlGraphVertex
{
public:
	map<string, Range> ranges;
public:
	virtual void Pack(ReadWriteBuffer& buf); // pack itself to buffer
	virtual void Unpack(ReadWriteBuffer& buf); // unpack itself from buffer
};

class VertexIf : public ControlGraphVertex
{
};

class VertexWhile : public ControlGraphVertex
{
};*/

}
