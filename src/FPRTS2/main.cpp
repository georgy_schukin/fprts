#include "fprts.h"
#include "test.h"

#include <stdio.h>

using namespace fprts;

int main()
{
	FPRTS rts;

	printf("Init...\n");
	rts.Init();

	double t = MPI_Wtime();

	printf("Load program...\n");	
	FragmentId code_id1 = rts.RegisterCode<fptest::Test1>("Test1");
	rts.LoadProgram(0, code_id1, vector<int>(0));
	rts.LoadProgram(1, code_id1, vector<int>(0));

	printf("Run...\n");
	rts.Run(2);

	printf("Shutdown...\n");
	rts.Shutdown();

	printf("Total time %.5f\n", MPI_Wtime() - t);

	printf("Exit...\n");
	rts.Finalize();

return 0;
}
