#include "fragmentprocstorage.h"

namespace fprts {

FragmentProcedure* FragmentProcStorage::NewProc() {		
	return new FragmentProcedure(ProcedureId(rank, proc_cnt++));
}

void FragmentProcStorage::AddProc(FragmentProcedure *proc) {
	procs[proc->GetId()] = boost::shared_ptr<FragmentProcedure>(proc);
}

FragmentProcedure* FragmentProcStorage::GetProc(ProcedureId id) {	
	if(procs.find(id) == procs.end()) { // no proc - create it
		procs[id] = boost::shared_ptr<FragmentProcedure>(new FragmentProcedure(id));
	}
return procs[id].get();
}

void FragmentProcStorage::RemoveProc(ProcedureId id) {
	procs.erase(id);
}

bool FragmentProcStorage::ProcExists(ProcedureId id) const {
	return(procs.find(id) != procs.end());
}

}